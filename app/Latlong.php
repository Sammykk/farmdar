<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Latlong extends Model
{
    protected $table = 'latlong';
    protected $fillable = ['fields_id','user_id', 'lat_long', 'report','acreage'];
}
