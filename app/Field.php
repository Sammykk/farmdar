<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $fillable = ['field_name', 'crop_stage', 'crop_type', 'report', 'acreage', 'vertices', 'status', 'create', 'observer_id'];

}
