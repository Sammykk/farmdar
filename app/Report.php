<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = ['report_type', 'group_id', 'device_id', 'timestamp', 'paddockNames', 'lat', 'lng', 'model', 'Nmin', 'SOC', 'pH', 'P', 'K', 'moisture', 'polygons'];
}
