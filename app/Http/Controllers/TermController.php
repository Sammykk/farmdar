<?php

namespace App\Http\Controllers;

use App\Term;
use Illuminate\Http\Request;

class TermController extends Controller
{
    public function show(){
        $terms = Term::select('term')->limit(1)->get();
        return view('pages.term', ['terms' => $terms]);
    }
}
