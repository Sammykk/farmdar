<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class MapController extends BaseController
{
    function yandex(){
    	return view('map.yandex');
    }

    function jvector(){
        return view('map.jvector');
    }

    function dashboard(){
        return view('overlay_user.dashboad.index');
    }

    function fields(){
        return view('overlay_user.fields.index');
    }

    function notifictions(){
        return view('overlay_user.notification.index');
    }

    function scan(){
        return view('overlay_user.requestsacn.index');
    }

    function map(){
        return view('overlay_user.map.index');
    }
}
