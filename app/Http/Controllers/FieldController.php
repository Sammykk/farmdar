<?php

namespace App\Http\Controllers;

use App\Field;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Latlong;
use App\Report;


class FieldController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $role = Auth::user()->role_id;
        if($role == 1){
            $fields = Field::where('status',1)->get();
            return view('overlay_user.fields.admin',compact('fields'));
        }else{
            $user_id = Auth::user()->id;
            $fields = Field::where('status',1)->where('create',$user_id)->get();
            return view('overlay_user.fields.index',compact('fields'));
        }

    }
    
    public function observer()
    {   
        $fields = Field::where('status', 1)->get();
        return view('overlay_user.fields.observer', compact('fields'));
    }

    /**
     * Show list of resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('overlay_user.map.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function created()
    {
        return view('overlay_user.map.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user_id = Auth::user()->id;
        $input = $request->except('_token');
        
        $input['create'] = $user_id;
        $input['status'] =  1;


        $data = new Field();


        $data->fill($input)->save();

        //return view('overlay_user.map.index');
        return redirect()->route('user.fields');

    }

    public function latstore(Request $request)
    {
        if(empty($request->report)){
            $user_id = Auth::user()->id;
            $input = $request->except('_token');

            $input['lat_long'] = json_encode($request->json);
            $input['user_id'] = $user_id;
            $input['report'] = "Main";

            $data = new Latlong();


            $data->fill($input)->save();

            return $data;
        }else{
            $user_id = Auth::user()->id;
            $input = $request->except('_token');

            $input['lat_long'] = json_encode($request->json);
            $input['user_id'] = $user_id;
            

            $data = new Latlong();


            $data->fill($input)->save();

            return $data;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Field  $field
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Field::findOrFail($id);

        return view('overlay_user.map.show', compact('data'));
    }
    
    public function display_report($id, Request $request)
    {
        $data = Field::findOrFail($id);
        $tss = $request->only('tss');
        $tss = implode(" ", $tss);
        
        $inp = $data['vertices'];
        $inp = str_replace("[{", "", $inp);
        $inp = str_replace("}]", "", $inp);
        $inp = str_replace("lat:", "", $inp);
        $inp = str_replace("lng:", "", $inp);
        $inp = explode("},{", $inp);
        $lat1 = 0; $lat2 = 0; $lng1 = 0; $lng2 = 0;
        for($i = 0; $i < count($inp); $i++){
            $tmp = explode(",", $inp[$i]);
            $tmp[0] = substr($tmp[0], 0, 6);
            $tmp[1] = substr($tmp[1], 0, 6);
            if($i == 0){
                $lat1 = $tmp[0];
                $lat2 = $tmp[0];
                $lng1 = $tmp[1];
                $lng2 = $tmp[1];
            }else{
                if($tmp[0] < $lat1){$lat1 = $tmp[0];}
                if($tmp[0] > $lat2){$lat2 = $tmp[0];}
                if($tmp[1] < $lng1){$lng1 = $tmp[1];}
                if($tmp[1] > $lng2){$lng2 = $tmp[1];}
            }
        }
        $lat = [];
        $a = 0;
        for($i = $lat1; $i <= $lat2; $i += 0.001){
            $lat[$a++] = $i;
        }
        $lng = [];
        $a = 0;
        for($i = $lng1; $i <= $lng2 + 0.001; $i += 0.001){
            $lng[$a++] = $i;
        }
        $ts = Report::where('polygons', 'LIKE', '%' . substr($data['center'], 0, 7) . '%')->distinct('timestamp')->orderby('timestamp', 'DESC')->get('timestamp');
        if(isset($ts[0])){
            $tx = $ts[0]['timestamp'];
            if(strlen($tss) > 2){
                $tx = $tss;
            }
            $report = Report::where('timestamp', $tx)
            ->where(function ($query) use ($lat) {
                for($i = 0; $i < count($lat); $i++){
                    $query->orWhere('polygons', 'LIKE', '%' . $lat[$i] . '%');
                }
            })->where(function ($query) use ($lng) {
                for($i = 0; $i < count($lng); $i++){
                    $query->orWhere('polygons', 'LIKE', '%' . $lng[$i] . '%');
                }
            })->get();
        }else{
            $report = Report::where(function ($query) use ($lat) {
                for($i = 0; $i < count($lat); $i++){
                    $query->orWhere('polygons', 'LIKE', '%' . $lat[$i] . '%');
                }
            })->where(function ($query) use ($lng) {
                for($i = 0; $i < count($lng); $i++){
                    $query->orWhere('polygons', 'LIKE', '%' . $lng[$i] . '%');
                }
            })->get();
        }
        return view('overlay_user.map.display_report', compact(array('data', 'report', 'ts')));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Field  $field
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Field::findOrFail($id);

        return view('overlay_user.map.edit',compact('data'));
    }
    
    public function report_show($id)
    {
        $data = Report::findOrFail($id);

        return view('overlay_user.report.show', compact('data'));
    }
    
    public function report_delete($id)
    {
        Report::destroy($id);

        return redirect()->back()->with('message', 'Record has been deleted');
    }
    
    public function report_edit($id)
    {
        $data = Report::findOrFail($id);

        return view('overlay_user.report.edit', compact('data'));
    }
    
    /**
     * Show the form for procesing the specified resource.
     *
     * @param  \App\Field  $field
     * @return \Illuminate\Http\Response
     */
    public function process($id)
    {
        $data = Field::findOrFail($id);

        return view('overlay_user.map.process',compact('data'));
    }
    
    /**
     * Delete the specified resource.
     *
     * @param  \App\Field  $field
     * @return \Illuminate\Http\Response
     */
    public function remove($id)
    {
        Field::destroy($id);

        return redirect()->back()->with('message', 'Record has been deleted');
    }

    public function store_account(Request $request)
    {
        if(Auth::user()->role_id != 1){
            return redirect()->back()->with('message', 'This operation is reserved for admins only');
        } else {
            $input = $request->except('_token');
            $input['created_at'] = date("Y-m-d h:i:s", time());
            $input['password'] = Hash::make($request->password);
            $data = new User();
            $data->fill($input)->save();
            return redirect()->route('user.accounts');
        }
    }
    
    public function generate_data($input, $content, $request_report_type, $main){
        $offset = strpos($content, "[") + 1;
        $range = 0;
        while($offset < strlen($content) - 4){
            $input['report_type'] = $request_report_type;
            $var = "] } }";
            $range = strpos($content, $var, $offset) + strlen($var);
            $var = '"ID": ';
            $id_st = strpos($content, $var, $offset) + strlen($var);
            $id_en = strpos($content, ',', $id_st);
            $input['group_id'] = substr($content, $id_st, $id_en - $id_st);
            $var = $main;
            $id_st = strpos($content, $var, $offset) + strlen($var);
            $id_en = strpos($content, ',', $id_st);
            $input['paddockNames'] = substr($content, $id_st, $id_en - $id_st);
            $var = '"Timestamp": "';
            $id_st = strpos($content, $var, $offset) + strlen($var);
            $id_en = strpos($content, '"', $id_st);
            $input['timestamp'] = substr($content, $id_st, $id_en - $id_st);
            $var = '"coordinates": ';
            $id_st = strpos($content, $var, $offset) + strlen($var);
            $tmp = substr($content, $id_st, 12);
            $las = strrpos($tmp, '[ ') + 2;
            $id_st = $id_st + $las;
            $out = '';
            while(true){
                $id_en = strpos($content, ' ]', $id_st);
                $arr = explode(", ", substr($content, $id_st, $id_en - $id_st));
                $out .= $arr[1] . "_" . $arr[0];
                $var = '[ ';
                $id_st = strpos($content, $var, $id_en) + strlen($var);
                if(substr($content, $id_en, 4) == " ] ]"){break;} else {$out .= ",";}
            }
            $input['polygons'] = $out;
            $offset = $range + 2;
            
            $data = new Report();
            $data->fill($input)->save();
        }   
    }
    
    public function create_report(Request $request)
    {
        $role = Auth::user()->role_id;
        if($role != 1){
            return redirect()->back()->with('message', 'This operation is reserved for admins only');
        } else {
            $input = $request->except('_token');
            $content = file_get_contents($request->report_file);
            if($request->report_type == "population"){
                $chunk = [];
                $offset = strpos($content, "[") + 1;
                $range = 0;
                while($offset < strlen($content) - 4){
                    $range = strpos($content, "] } }", $offset) + 5;
                    array_push($chunk, json_decode(substr($content, $offset, $range - $offset)));
                    $offset = $range + 2;
                }
                for($i = 0; $i < count($chunk); $i++){
                    $input['timestamp'] = $chunk[$i]->properties->Timestamp;
                    $input['report_type'] = $request->report_type;
                    $input['polygons'] = $chunk[$i]->geometry->coordinates[1] . '_' . $chunk[$i]->geometry->coordinates[0];
                    $data = new Report();
                    $data->fill($input)->save();
                }
            }
            if($request->report_type == "canopy"){
                $this->generate_data($input, $content, $request->report_type, '"group_colo": ');
            }
            if($request->report_type == "health"){
                $this->generate_data($input, $content, $request->report_type, '"Health": ');
            }
            if($request->report_type == "nitrogen_requirement"){
                $this->generate_data($input, $content, $request->report_type, '"Nitrogen": ');
            }
            if($request->report_type == "plant_stress"){
                $this->generate_data($input, $content, $request->report_type, '"Stress": ');
            }
            if($request->report_type == "water_stress"){
                $this->generate_data($input, $content, $request->report_type, '"WaterStress": ');
            }
            if($request->report_type == "soil"){
                $lines = explode(PHP_EOL, $content);
                $out = array();
                for($j = 0; $j < count($lines); $j++) {
                    $out[] = explode(",", $lines[$j]);
                }
                for($i = 1; $i < count($out) - 1; $i++){
                    $input['timestamp'] = date("Y-m-d");
                    $input['report_type'] = $request->report_type;
                    $input['group_id'] = $out[$i][0];
                    $input['paddockNames'] = $out[$i][3];
                    $input['polygons'] = $out[$i][4] . '_' . $out[$i][5];
                    $input['model'] = $out[$i][6];
                    $input['Nmin'] = $out[$i][8];
                    $input['SOC'] = $out[$i][9];
                    $input['pH'] = $out[$i][11];
                    $input['moisture'] = $out[$i][12];
                    $input['P'] = $out[$i][13];
                    $input['K'] = $out[$i][15];
                    $data = new Report();
                    $data->fill($input)->save();
                }
            }
            return redirect()->back()->with('message', 'Record has been updated successfully');
        }
    }
    
    public function field_update(Request $request)
    {
        $user_id = Auth::user()->id;
        $input = $request->except('_token');
        
        $data = Field::where('id',$request->field_id)->first();
        
        $data->update($input);
        return redirect()->back()->with('message', 'Record has been updated successfully');
    }
    
    public function update_report(Request $request)
    {
        $role = Auth::user()->role_id;
        if($role != 1){
            return redirect()->back()->with('message', 'This operation is reserved for admins only');
        } else {
            $user_id = Auth::user()->id;
            $input = $request->except('_token');
            
            $data = Report::where('id', $request->id)->first();
            
            $data->update($input);
            return redirect()->back()->with('message', 'Record has been updated successfully');
        }
    }

    public function report(){
        $role = Auth::user()->role_id;
        if($role != 1){
            return redirect()->back()->with('message', 'This data is reserved for admins only');
        } else {
            return view('overlay_user.report.index');
        }
    }
    
    public function report_dashboard()
    {
        return view('overlay_user.report.dashboard');
    }

    public function profile(Request $request){

        if($request->id == null){
            $user_id = Auth::user()->id;
            $data = User::where('id', $user_id)->first();
        } else {
            $user_id = $request->id;
            $data = User::where('id', $user_id)->first();
        }

        return view('overlay_user.profile.index',compact('data'));
    }
    
    public function accounts(){
        
        if(Auth::user()->role_id != 1){
            return redirect()->back()->with('message', 'This data is reserved for admins only');
        } else {
            $data = User::where('id', '!=', Auth::user()->id)->get();
            return view('overlay_user.profile.list', compact('data'));
        }
    }
    
    public function create_account()
    {
        return view('overlay_user.profile.create');
    }
    
    public function profileedit(Request $request){


        $user_id = Auth::user()->id;
        $data = User::where('id',$user_id)->first();
        $input = $request->except('_token');
        // $input = $request->except('email');

        if(!empty($request->password)){
            $input['password'] = Hash::make($request->password);
        }else{
            $input['password'] = $data->password;
        }

        $data->update($input);

        return view('overlay_user.profile.index',compact('data'))->withSuccess('Record has been updated successfully');
    }

    public function sample(){

        return view('overlay_user.sample.index');
    }
}
