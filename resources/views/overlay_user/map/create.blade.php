@extends('user_layout.master')
@section('title', 'Fields')
@section('parentPageTitle', 'Map')
@section('page-style')
    <link rel="stylesheet" href="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.css') }}" />
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script src="https://cdn.jsdelivr.net/npm/exif-js"></script>
@stop
@section('content')
    <div class="row">
        <div class="col-log-8">
            <div class="alert alert-primary" role="alert">
                <div class="container">
                    <p class="info" id="info">Find your fields by typing the location in the address bar.</p>
                    <p class="info" id="infos"></p>
                </div>
            </div>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-12 col-lg-8">
            <div class="card">
                <input id="pac-input" onclick="toggleClock()" type=" text" class="form-control" value=""
                    placeholder="Enter Your Address Here"
                    style="border-style: solid; border-color: red;  border-radius: 25px;">
                <div id="map" style="width:100%; height:450px;"></div>
                <form method="post" action="{{ route('fiels-store') }}">
                    @csrf
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="field_name" placeholder="Field Name" required />
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="area" readonly name="acreage" placeholder="Acreage" value="" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="crop_type" placeholder="Crop Stage" required />
                                </div>
                                <div class="form-group">
                                    <select name="crop_stage" id="crop_stage" class="form-control" required>
                                        <option value="">Select Growing Stage</option>
                                        <option value="Mature">Mature</option>
                                        <option value="Harvest">Harvest</option>
                                        <option value="Seeding">Seeding</option>
                                        <option value="Intermediate">Intermediate</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="hidden" class="form-control" name="vertices" value="" id="vertices" />
                                    <input type="hidden" class="form-control report" name="report" value="" id="report" />
                                    <button class="btn btn-primary"> Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12">
            <div class="card">
                {{-- <div class="header">
                    <h2><strong>Select Reports</strong></h2>
                    <select name="report" id="reports" class="form-control " onchange="myFunction()" required>
                        <option value="">Select Report</option>
                        <option value="1">Nitrogen</option>
                        <option value="2">Plant Count</option>
                        <option value="3">Water Stress</option>
                        <option value="4">Plant Health</option>
                        <option value="5">Evapo Transp</option>
                    </select>
                </div> --}}
                <div class="" style=" width:100%; height:400px; border-style: solid; border-color: black; border-radius: 25px; background-color: white;">
                    <table class="table" style="margin: 5px;">
                        <thead>
                            <th>Acreage Area</th>
                        </thead>
                        <tbody>
                            <td><input type="text" class="form-control" id="areas" readonly name="" placeholder="Acreage" value="" /></td>
                        </tbody>
                    </table>
                    <table class="table" style="margin: 5px;">
                        <thead>
                            <th>lat & lang</th>
                        </thead>
                        <tbody>
                            <td><textarea name="" id="verticess" cols="30" rows="8" readonly></textarea></td>
                        </tbody>
                    </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-script')
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="{{ asset('assets/bundles/jvectormap.bundle.js') }}"></script>
    <script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-in-mill.js') }}"></script>
    <script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-us-aea-en.js') }}"></script>
    <script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-uk-mill-en.js') }}"></script>
    <script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-au-mill.js') }}"></script>
    <script src="{{ asset('assets/js/pages/maps/jvectormap.js') }}"></script>
    <script src="{{ asset('assets/plugins/momentjs/moment.js') }}"></script>
    <script
        src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}">
    </script>
    <script src="{{ asset('assets/js/pages/forms/basic-form-elements.js') }}"></script>

    <script>
        function initMap() {
            var map = new google.maps.Map(document.getElementById("map"), {
                zoom: 18,
                center: {
                    lat: 37.783,
                    lng: -122.403
                },
            });
            var bounds = {
                17: [
                    [20969, 20970],
                    [50657, 50658],
                ],
                18: [
                    [41939, 41940],
                    [101315, 101317],
                ],
                19: [
                    [83878, 83881],
                    [202631, 202634],
                ],
                20: [
                    [167757, 167763],
                    [405263, 405269],
                ],
            };
            var imageMapType = new google.maps.ImageMapType({
                getTileUrl: function(coord, zoom) {
                    if (
                        zoom < 17 ||
                        zoom > 20 ||
                        bounds[zoom][0][0] > coord.x ||
                        coord.x > bounds[zoom][0][1] ||
                        bounds[zoom][1][0] > coord.y ||
                        coord.y > bounds[zoom][1][1]
                    ) {
                        return "";
                    }
                    return [
                        "https://www.gstatic.com/io2010maps/tiles/5/L2_",
                        zoom,
                        "_",
                        coord.x,
                        "_",
                        coord.y,
                        ".png",
                    ].join("");
                },
                tileSize: new google.maps.Size(256, 256),
            });
            map.overlayMapTypes.push(imageMapType);
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyEYDW1fvNqG86HXoKuNsSxXJtlWtUG9k&callback=initMap" async></script>


@stop
