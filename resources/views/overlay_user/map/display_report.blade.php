@extends('user_layout.master')
@section('title', 'Display Reports')
@section('parentPageTitle', 'Map')
@section('page-style')
    <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-select/css/bootstrap-select.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/plugins/charts-c3/plugin.css')}}"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyEYDW1fvNqG86HXoKuNsSxXJtlWtUG9k&callbackv=3.exp&sensor=false&libraries=drawing,geometry,places&scroll=true"></script>
    <style>
        #color-palette {
            clear: both;
        }
        .color-button {
            width: 14px;
            height: 14px;
            font-size: 0;
            margin: 2px;
            float: left;
            cursor: pointer;
        }
        #map {
            height: 80vh;
            width: 100%;
        }
        .block-header{
            display: none;
        }
        #ls{
            background-color: rgba(255, 255, 255, 0.5);
            font-size: 14px;
            padding-top: 16px;
            padding-right: 20px;
            border-radius: 10px;
            margin-left: 10px;
            margin-top: 10px;
        }
        #dt{
            width: 20%;
            background-color: rgba(255, 255, 255, 0.5);
            font-size: 14px;
            padding: 16px;
            border-radius: 10px;
            margin-right: 10px;
            text-align: center;
            margin-top: 10px;
        }
        #ls li{
            margin-bottom: 5px;
            cursor: pointer;
        }
    </style>
@stop
@section('content')
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <div id="ls">
                    <ul>
                        <li onclick="soil('count')" id="count_link" style="display: none;">Plant Count</li>
                        <li onclick="soil('canopy')" id="canopy_link" style="display: none;">Canopy Cover</li>
                        <li onclick="soil('health')" id="health_link" style="display: none;">Plant Health</li>
                        <li onclick="soil('nitrogen_requirement')" id="nitrogen_requirement_link" style="display: none;">Nitrogen Requirement</li>
                        <li onclick="soil('water_stress')" id="water_stress_link" style="display: none;">Water Stress Levels</li>
                        <li onclick="soil('plant_stress')" id="plant_stress_link" style="display: none;">Plant Stress</li>
                        <li onclick="soil('overview')" id="soil_link" style="display: none;">Soil Analysis</li>
                        <ul>
                            <li onclick="soil('nitro')" id="nitro_link" style="display: none;">Nitrogen (N)</li>
                            <li onclick="soil('p')" id="p_link" style="display: none;">Phosphorous (P)</li>
                            <li onclick="soil('k')" id="k_link" style="display: none;">Potassium (K)</li>
                            <li onclick="soil('soc')" id="soc_link" style="display: none;">Soil Organic Carbon (SOC)</li>
                            <li onclick="soil('ph')" id="ph_link" style="display: none;">pH Level</li>
                            <li onclick="soil('moisture')" id="moisture_link" style="display: none;">Moisture</li>
                        </ul>
                    </ul>
                </div>
                <div id="dt"></div>
                <div id="map"></div>
                <div id="dts"></div>
            </div>
        </div>
    </div>
@stop

@section('page-script')

    <script src="{{ asset('assets/plugins/momentjs/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/js/pages/forms/basic-form-elements.js') }}"></script>

    <script>
    
        var arr = '{{ $report }}';
        arr = arr.replaceAll("&quot;", "\"");
        arr = JSON.parse(arr);
        var arr2 = [];
        var arr3 = [];
        var arr4 = [];
        var arr5 = [];
        var arr6 = [];
        var arr7 = [];
        var arr8 = [];
        
        const coord = {{ $data->vertices }};
        var acreage = {{ $data->acreage }};
        var ts = '{{ $ts }}';
        ts = ts.replaceAll("&quot;", "\"");
        ts = JSON.parse(ts);
        var tss = "<table style='width: 100%; border-collapse: separate;'><tr>";
        for(var i = 0; i < ts.length; i++){
            tss += "<td style='text-align: center; border: 2px solid #aaa; border-radius: 8px;'><a class='button' href='{{ url('user/fields/display_report/'.$data->id)}}?tss=" + ts[i].timestamp + "'>" + ts[i].timestamp + "</a></td>";
        }
        tss += "</tr></table>";
        $('#dts').html(tss);
        
        const farm = new google.maps.Polygon({
            paths: coord,
            strokeColor: "#FFFFFF",
            strokeOpacity: 1,
            strokeWeight: 2,
            fillColor: "#FFFFFF",
            fillOpacity: 0,
        });
        
        var art = [];
        for(var i = 0; i < arr.length; i++){
            var pox = arr[i].polygons.split(",");
            if(pox.length == 0){
                var cor = arr[i].polygons.split("_");
            }else{
                var cor = pox[0].split("_");
            }
            var point = new google.maps.LatLng(cor[0], cor[1]);
            if(google.maps.geometry.poly.containsLocation(point, farm)){
                art.push(arr[i]);
            }
        }
        arr = art;
        
        var current_report = "";
        for(var i = 0; i < arr.length; i++){
            if(arr[i]['report_type'] == "population"){
                arr2.push(arr[i]);
                $("#count_link").show();
            }
            if(arr[i]['report_type'] == "soil"){
                arr3.push(arr[i]);
                $("#soil_link").show();
                $("#nitro_link").show();
                $("#p_link").show();
                $("#k_link").show();
                $("#soc_link").show();
                $("#ph_link").show();
                $("#moisture_link").show();
            }
            if(arr[i]['report_type'] == "canopy"){
                arr4.push(arr[i]);
                $("#canopy_link").show();
            }
            if(arr[i]['report_type'] == "health"){
                arr5.push(arr[i]);
                $("#health_link").show();
            }
            if(arr[i]['report_type'] == "water_stress"){
                arr6.push(arr[i]);
                $("#water_stress_link").show();
            }
            if(arr[i]['report_type'] == "nitrogen_requirement"){
                arr7.push(arr[i]);
                $("#nitrogen_requirement_link").show();
            }
            if(arr[i]['report_type'] == "plant_stress"){
                arr8.push(arr[i]);
                $("#plant_stress_link").show();
            }
        }
        arr = arr3;
        
        var markers = [];
        var polygons = [];
        
        var sma = 0;
        var smo = 0;
        for(var i = 0; i < coord.length; i++){
            smo += coord[i][0];
            sma += coord[i][1];
        }
        smo = smo / coord.length;
        sma = sma / coord.length;
        smo = smo.toFixed(14);
        sma = sma.toFixed(14);
        
        var starting_zoom = 21;
        if(acreage <= 2){ starting_zoom = 21; }
        if(acreage > 2 && acreage <= 30){ starting_zoom = 19; }
        if(acreage > 30 && acreage <= 60){ starting_zoom = 18; }
        if(acreage > 60 && acreage <= 100){ starting_zoom = 17; }
        if(acreage > 100){ starting_zoom = 16; }
        var bounds = new google.maps.LatLngBounds();
        var i;
        for (i = 0; i < coord.length; i++) {
            bounds.extend(coord[i]);
        }
        var ctr = bounds.getCenter();
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: starting_zoom,
            center: ctr,
            mapTypeId: google.maps.MapTypeId.SATELLITE,
            labels: true,
            zoomControl: true,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.LEFT_BOTTOM,
            },
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: false
        });
        
        var imageBounds = {
            north: 28.4730504,
            south: 28.4691529,
            east: 70.0789256,
            west: 70.0745442
        };
        var historicalOverlay = new google.maps.GroundOverlay("{{ asset('assets/images/gari_bela.png') }}", imageBounds);
        historicalOverlay.setMap(map);
        
        hideMarkers();
        markers = [];
        for(var i = 0; i < arr.length; i++){
            var point = new google.maps.LatLng(arr[i].lat, arr[i].lng);
            if(google.maps.geometry.poly.containsLocation(point, farm)){
                createMarkers(map, point, "blue", "Soil Average");
            }
        }
        map.controls[google.maps.ControlPosition.LEFT_TOP].push(document.getElementById('ls'));
        map.controls[google.maps.ControlPosition.RIGHT_TOP].push(document.getElementById('dt'));
        farm.setMap(map);
        
        function createMarkers(map, coords, color, label) {
            markers.push(
                new google.maps.Marker({
                    position: coords,
                    map: map,
                    title: label,
                    icon: {
                        path: google.maps.SymbolPath.CIRCLE,
                        fillColor: color,
                        fillOpacity: 0.1,
                        strokeColor: "white",
                        strokeWeight: 1,
                        scale: 4,
                    }
                })
            );
        }
        function createPolygons(map, pol, col) {
            polygons.push(
                new google.maps.Polygon({
                    paths: pol,
                    map: map,
                    strokeColor: col,
                    strokeOpacity: 0.7,
                    strokeWeight: 0,
                    fillColor: col,
                    fillOpacity: 0.3,
                })
            );
        }
        function soil(inp){
            if(inp == "count" && current_report != "count"){
                current_report = "count";
                for(var i = 0; i < markers.length; i++){ markers[i].setMap(null); }
                markers = [];
                for(var i = 0; i < polygons.length; i++){ polygons[i].setMap(null); }
                polygons = [];
                var cont = "<b>Population: </b>" + arr2.length;
                hideMarkers();
                markers = [];
                for(var i = 0; i < arr2.length; i++){
                    var pox = arr2[i].polygons.split("_");
                    var point = new google.maps.LatLng(pox[0], pox[1]);
                    if(google.maps.geometry.poly.containsLocation(point, farm)){
                        createMarkers(map, point, "#e31a1c", "number: " + i);
                    }
                }
                farm.setMap(map);
                $("#dt").html(cont);
            }
            if(inp == "canopy" && current_report != "canopy"){
                current_report = "canopy";
                for(var i = 0; i < markers.length; i++){ markers[i].setMap(null); }
                markers = [];
                for(var i = 0; i < polygons.length; i++){ polygons[i].setMap(null); }
                polygons = [];
                var cont = "<b>Canopy Cover</b><br />";
                var canopy = 0;
                var no_canopy = 0;
                for(let i in arr4){
                    if(arr4[i]['group_id'] == "No Canopy"){canopy++;}
                    if(arr4[i]['group_id'] == "Canopy"){no_canopy++;}
                    var pol = arr4[i]['polygons'].split(",");
                    if(pol.length == 0){
                        var tmp = arr4[i]['polygons'].split("_");
                        pol[0] = {lat: parseFloat(tmp[0]),lng: parseFloat(tmp[1])};
                    }else{
                        for(var j in pol){
                            var tmp = pol[j].split("_");
                            pol[j] = {lat: parseFloat(tmp[0]),lng: parseFloat(tmp[1])};
                        }
                    }
                    createPolygons(map, pol, "#" + arr4[i]['paddockNames']);
                    farm.setMap(map);
                }
                cont += "Canopy (Green): " + ((canopy / (canopy + no_canopy)) * 100).toFixed(2) + "%<br />";
                cont += "No Canopy (Red): " + ((no_canopy / (canopy + no_canopy)) * 100).toFixed(2) + "%";
                cont += "<div id='chart-pie' class='text-left'></div>";
                $(document).ready(function(){
                    var chart = c3.generate({
                        bindto: '#chart-pie',
                        data: {
                            columns: [
                                ['canopy', canopy],
                                ['no_canopy', no_canopy]
                            ],
                            type: 'pie',
                            colors: {
                                'canopy': Aero.colors["#33d82c"],
                                'no_canopy': Aero.colors["#e31a1c"]
                            },
                            names: {
                                'canopy': 'Canopy',
                                'no_canopy': 'No Canopy'
                            }
                        },
                        axis: {
                        },
                        legend: {
                            show: true,
                        },
                        padding: {
                            bottom: 0,
                            top: 0
                        },
                    });
                });
                $("#dt").html(cont);
            }
            if(inp == "health" && current_report != "health"){
                current_report = "health";
                for(var i = 0; i < markers.length; i++){ markers[i].setMap(null); }
                markers = [];
                for(var i = 0; i < polygons.length; i++){ polygons[i].setMap(null); }
                polygons = [];
                var cont = "<b>Plant Health</b><div id='chart-pie' class='text-left'></div><table style='width: 100%;'>";
                var level_1 = 0;
                var level_2 = 0;
                var level_3 = 0;
                var level_4 = 0;
                for(var i in arr5){
                    var color, label;
                    if(arr5[i]['paddockNames'] >= -1 && arr5[i]['paddockNames'] <= 0.5){level_1++; color = "#fc5736"; label = "Low / No Vegetation";}
                    if(arr5[i]['paddockNames'] >= 0.5 && arr5[i]['paddockNames'] <= 0.7){level_2++; color = "#fcec36"; label = "Moderate";}
                    if(arr5[i]['paddockNames'] >= 0.7 && arr5[i]['paddockNames'] <= 0.9){level_3++; color = "#57fc36"; label = "Good";}
                    if(arr5[i]['paddockNames'] >= 0.9 && arr5[i]['paddockNames'] <= 1){level_4++; color = "#2e3fff"; label = "Excellent";}
                    
                    var pol = arr5[i]['polygons'].split(",");
                    for(var j in pol){
                        var tmp = pol[j].split("_");
                        pol[j] = {lat: parseFloat(tmp[0]), lng: parseFloat(tmp[1])};
                    }
                    createPolygons(map, pol, color);
                    farm.setMap(map);
                }
                $(document).ready(function(){
                    var chart = c3.generate({
                        bindto: '#chart-pie',
                        data: {
                            columns: [
                                ['level_1', level_1],
                                ['level_2', level_2],
                                ['level_3', level_3],
                                ['level_4', level_4]
                            ],
                            type: 'pie',
                            colors: {
                                'level_1': "#fc5736",
                                'level_2': "#fcec36",
                                'level_3': "#57fc36",
                                'level_4': "#2e3fff"
                            },
                            names: {
                                'level_1': 'Low / No Vegetation',
                                'level_2': 'Moderate',
                                'level_3': 'Good',
                                'level_4': 'Excellent'
                            }
                        },
                        axis: {
                        },
                        legend: {
                            show: true,
                        },
                        padding: {
                            bottom: 0,
                            top: 0
                        },
                    });
                });
                cont += "<tr><td class='text-left'>Low / No Vegetation</td><td class='text-right'>"+((level_1/arr5.length)*100).toFixed(2)+"%</td></tr>";
                cont += "<tr><td class='text-left'>Moderate</td><td class='text-right'>"+((level_2/arr5.length)*100).toFixed(2)+"%</td></tr>";
                cont += "<tr><td class='text-left'>Good</td><td class='text-right'>"+((level_3/arr5.length)*100).toFixed(2)+"%</td></tr>";
                cont += "<tr><td class='text-left'>Excellent</td><td class='text-right'>"+((level_4/arr5.length)*100).toFixed(2)+"%</td></tr>";
                cont += "</table>";
                $("#dt").html(cont);
            }
            if(inp == "water_stress" && current_report != "water_stress"){
                current_report = "water_stress";
                for(var i = 0; i < markers.length; i++){ markers[i].setMap(null); }
                markers = [];
                for(var i = 0; i < polygons.length; i++){ polygons[i].setMap(null); }
                polygons = [];
                var cont = "<b>Water Stress Levels</b><div id='chart-pie' class='text-left'></div><table style='width: 100%;'>";
                var level_1 = 0;
                var level_2 = 0;
                var level_3 = 0;
                var level_4 = 0;
                for(var i in arr6){
                    var color, label;
                    if(arr6[i]['paddockNames'] >= -1 && arr6[i]['paddockNames'] <= 0.2){level_1++; color = "#fc5736"; label = "Stress / No Vegetation";}
                    if(arr6[i]['paddockNames'] >= 0.2 && arr6[i]['paddockNames'] <= 0.3){level_2++; color = "#fcd6cf"; label = "Mild Stress";}
                    if(arr6[i]['paddockNames'] >= 0.3 && arr6[i]['paddockNames'] <= 0.4){level_3++; color = "#60afe4"; label = "No Stress";}
                    if(arr6[i]['paddockNames'] >= 0.4 && arr6[i]['paddockNames'] <= 1){level_4++; color = "#2e3fff"; label = "Good Moisture";}
                    
                    var pol = arr6[i]['polygons'].split(",");
                    for(var j in pol){
                        var tmp = pol[j].split("_");
                        pol[j] = {lat: parseFloat(tmp[0]), lng: parseFloat(tmp[1])};
                    }
                    createPolygons(map, pol, color);
                    farm.setMap(map);
                }
                $(document).ready(function(){
                    var chart = c3.generate({
                        bindto: '#chart-pie',
                        data: {
                            columns: [
                                ['level_1', level_1],
                                ['level_2', level_2],
                                ['level_3', level_3],
                                ['level_4', level_4]
                            ],
                            type: 'pie',
                            colors: {
                                'level_1': "#fc5736",
                                'level_2': "#fcd6cf",
                                'level_3': "#60afe4",
                                'level_4': "#2e3fff"
                            },
                            names: {
                                'level_1': 'Stress / No Vegetation',
                                'level_2': 'Mild Stress',
                                'level_3': 'No Stress',
                                'level_4': 'Good Moisture'
                            }
                        },
                        axis: {
                        },
                        legend: {
                            show: true,
                        },
                        padding: {
                            bottom: 0,
                            top: 0
                        },
                    });
                });
                cont += "<tr><td class='text-left'>Stress / No Vegetation</td><td class='text-right'>"+((level_1/arr6.length)*100).toFixed(2)+"%</td></tr>";
                cont += "<tr><td class='text-left'>Mild Stress</td><td class='text-right'>"+((level_2/arr6.length)*100).toFixed(2)+"%</td></tr>";
                cont += "<tr><td class='text-left'>No Stress</td><td class='text-right'>"+((level_3/arr6.length)*100).toFixed(2)+"%</td></tr>";
                cont += "<tr><td class='text-left'>Good Moisture</td><td class='text-right'>"+((level_4/arr6.length)*100).toFixed(2)+"%</td></tr>";
                cont += "</table>";
                $("#dt").html(cont);
            }
            if(inp == "nitrogen_requirement" && current_report != "nitrogen_requirement"){
                current_report = "nitrogen_requirement";
                for(var i = 0; i < markers.length; i++){ markers[i].setMap(null); }
                markers = [];
                for(var i = 0; i < polygons.length; i++){ polygons[i].setMap(null); }
                polygons = [];
                var cont = "<b>Nitrogen Requirement</b><div id='chart-pie' class='text-left'></div><table style='width: 100%;'>";
                var level_1 = 0;
                var level_2 = 0;
                var level_3 = 0;
                for(var i in arr7){
                    var color, label;
                    if(arr7[i]['paddockNames'] >= -1 && arr7[i]['paddockNames'] <= 0.45){level_1++; color = "#fc5736"; label = "Low / No Vegetation";}
                    if(arr7[i]['paddockNames'] >= 0.45 && arr7[i]['paddockNames'] <= 0.55){level_2++; color = "#fcec36"; label = "OK";}
                    if(arr7[i]['paddockNames'] >= 0.55 && arr7[i]['paddockNames'] <= 1){level_3++; color = "#57fc36"; label = "High";}
                    
                    var pol = arr7[i]['polygons'].split(",");
                    for(var j in pol){
                        var tmp = pol[j].split("_");
                        pol[j] = {lat: parseFloat(tmp[0]), lng: parseFloat(tmp[1])};
                    }
                    createPolygons(map, pol, color);
                    farm.setMap(map);
                }
                $(document).ready(function(){
                    var chart = c3.generate({
                        bindto: '#chart-pie',
                        data: {
                            columns: [
                                ['level_1', level_1],
                                ['level_2', level_2],
                                ['level_3', level_3]
                            ],
                            type: 'pie',
                            colors: {
                                'level_1': "#fc5736",
                                'level_2': "#fcec36",
                                'level_3': "#57fc36"
                            },
                            names: {
                                'level_1': 'Low / No Vegetation',
                                'level_2': 'OK',
                                'level_3': 'High'
                            }
                        },
                        axis: {
                        },
                        legend: {
                            show: true,
                        },
                        padding: {
                            bottom: 0,
                            top: 0
                        },
                    });
                });
                cont += "<tr><td class='text-left'>Low / No Vegetation</td><td class='text-right'>"+((level_1/arr7.length)*100).toFixed(2)+"%</td></tr>";
                cont += "<tr><td class='text-left'>OK</td><td class='text-right'>"+((level_2/arr7.length)*100).toFixed(2)+"%</td></tr>";
                cont += "<tr><td class='text-left'>High</td><td class='text-right'>"+((level_3/arr7.length)*100).toFixed(2)+"%</td></tr>";
                cont += "</table>";
                $("#dt").html(cont);
            }
            if(inp == "plant_stress" && current_report != "plant_stress"){
                current_report = "plant_stress";
                for(var i = 0; i < markers.length; i++){ markers[i].setMap(null); }
                markers = [];
                for(var i = 0; i < polygons.length; i++){ polygons[i].setMap(null); }
                polygons = [];
                var cont = "<b>Plant Stress Levels</b><div id='chart-pie' class='text-left'></div><table style='width: 100%;'>";
                var level_1 = 0;
                var level_2 = 0;
                var level_3 = 0;
                var level_4 = 0;
                for(var i in arr8){
                    var color, label;
                    if(arr8[i]['paddockNames'] >= -1 && arr8[i]['paddockNames'] <= 4){level_1++; color = "#fc5736"; label = "Stress / No Vegetation";}
                    if(arr8[i]['paddockNames'] >= 4 && arr8[i]['paddockNames'] <= 6){level_2++; color = "#fcec36"; label = "Potential Stress";}
                    if(arr8[i]['paddockNames'] >= 6 && arr8[i]['paddockNames'] <= 8){level_3++; color = "#57fc36"; label = "No Stress";}
                    if(arr8[i]['paddockNames'] >= 8){level_4++; color = "#2e3fff"; label = "High Vigour";}
                    
                    var pol = arr8[i]['polygons'].split(",");
                    for(var j in pol){
                        var tmp = pol[j].split("_");
                        pol[j] = {lat: parseFloat(tmp[0]), lng: parseFloat(tmp[1])};
                    }
                    createPolygons(map, pol, color);
                    farm.setMap(map);
                }
                $(document).ready(function(){
                    var chart = c3.generate({
                        bindto: '#chart-pie',
                        data: {
                            columns: [
                                ['level_1', level_1],
                                ['level_2', level_2],
                                ['level_3', level_3],
                                ['level_4', level_4]
                            ],
                            type: 'pie',
                            colors: {
                                'level_1': "#fc5736",
                                'level_2': "#fcec36",
                                'level_3': "#57fc36",
                                'level_4': "#2e3fff"
                            },
                            names: {
                                'level_1': 'Stress / No Vegetation',
                                'level_2': 'Potential Stress',
                                'level_3': 'No Stress',
                                'level_4': 'High Vigour'
                            }
                        },
                        axis: {
                        },
                        legend: {
                            show: true,
                        },
                        padding: {
                            bottom: 0,
                            top: 0
                        },
                    });
                });
                cont += "<tr><td class='text-left'>Stress / No Vegetation</td><td class='text-right'>"+((level_1/arr8.length)*100).toFixed(2)+"%</td></tr>";
                cont += "<tr><td class='text-left'>Potential Stress</td><td class='text-right'>"+((level_2/arr8.length)*100).toFixed(2)+"%</td></tr>";
                cont += "<tr><td class='text-left'>No Stress</td><td class='text-right'>"+((level_3/arr8.length)*100).toFixed(2)+"%</td></tr>";
                cont += "<tr><td class='text-left'>High Vigour</td><td class='text-right'>"+((level_4/arr8.length)*100).toFixed(2)+"%</td></tr>";
                cont += "</table>";
                $("#dt").html(cont);
            }
            if(inp == "overview" && current_report != "overview"){
                for(var i = 0; i < markers.length; i++){ markers[i].setMap(null); }
                markers = [];
                for(var i = 0; i < polygons.length; i++){ polygons[i].setMap(null); }
                polygons = [];
                current_report = "overview";
                var ov = "<b>Soil Average Values</b><table style='width: 100%;'>";
                var nitro = 0;
                var soc = 0;
                var ph = 0;
                var moisture = 0;
                var p = 0;
                var k = 0;
                for(var i = 0; i < arr.length; i++){
                    nitro += parseFloat(arr[i].Nmin);
                    soc += parseFloat(arr[i].SOC);
                    ph += parseFloat(arr[i].pH);
                    moisture += parseFloat(arr[i].moisture);
                    p += parseFloat(arr[i].P);
                    k += parseFloat(arr[i].K);
                }
                ov += "<tr><td class='text-left'>Nitrogen (N)</td><td class='text-right'>"+(nitro/arr.length).toFixed(2)+"</td></tr>";
                ov += "<tr><td class='text-left'>Phosphorous (P)</td><td class='text-right'>"+(p/arr.length).toFixed(2)+"</td></tr>";
                ov += "<tr><td class='text-left'>Potassium (K)</td><td class='text-right'>"+(k/arr.length).toFixed(2)+"</td></tr>";
                ov += "<tr><td class='text-left'>Soil Organic Carbon (SOC)</td><td class='text-right'>"+(soc/arr.length).toFixed(2)+"</td></tr>";
                ov += "<tr><td class='text-left'>pH Level</td><td class='text-right'>"+(ph/arr.length).toFixed(2)+"</td></tr>";
                ov += "<tr><td class='text-left'>Moisture</td><td class='text-right'>"+(moisture/arr.length).toFixed(2)+"</td></tr>";
                ov += "</table>";
                $("#dt").html(ov);
            }
            if(inp == "nitro" && current_report != "nitro"){
                for(var i = 0; i < markers.length; i++){ markers[i].setMap(null); }
                markers = [];
                for(var i = 0; i < polygons.length; i++){ polygons[i].setMap(null); }
                polygons = [];
                current_report = "nitro";
                var ov = "<table style='width: 100%;'>";
                var nitro = 0;
                for(var i = 0; i < arr.length; i++){
                    if(arr[i].Nmin != null){
                        nitro += parseFloat(arr[i].Nmin);
                    }
                }
                ov += "<tr><td class='text-left'>Nitrogen Average Value</td><td class='text-right'>"+(nitro/arr.length).toFixed(2)+"</td></tr>";
                ov += "</table>";
                
                ov += "<div id='chart-pie' class='text-left'></div><table style='width: 100%;'>";
                var high = 0;
                var ok = 0;
                var low = 0;
                hideMarkers();
                markers = [];
                for(var i = 0; i < arr.length; i++){
                    var color, label;
                    if(arr[i].Nmin > 30){high++; color = "#fc5736"; label = "high";}
                    if(arr[i].Nmin >= 15 && arr[i].Nmin <= 30){ok++; color = "#57fc36"; label = "ok";}
                    if(arr[i].Nmin < 15){low++; color = "#fcec36"; label = "low";}
                    var tmp = arr[i].polygons.split("_");
                    var point = new google.maps.LatLng(tmp[0], tmp[1]);
                    if(google.maps.geometry.poly.containsLocation(point, farm)){
                        createMarkers(map, point, color, arr[i].Nmin);
                    }
                }
                farm.setMap(map);
                $(document).ready(function(){
                    var chart = c3.generate({
                        bindto: '#chart-pie',
                        data: {
                            columns: [
                                ['high', high],
                                ['ok', ok],
                                ['low', low]
                            ],
                            type: 'pie',
                            colors: {
                                'high': "#fc5736",
                                'ok': "#57fc36",
                                'low': "#fcec36"
                            },
                            names: {
                                'high': 'High',
                                'ok': 'OK',
                                'low': 'Low'
                            }
                        },
                        axis: {
                        },
                        legend: {
                            show: true,
                        },
                        padding: {
                            bottom: 0,
                            top: 0
                        },
                    });
                });
                ov += "<tr><td class='text-left'>High</td><td class='text-right'>"+((high/arr.length)*100).toFixed(2)+"%</td></tr>";
                ov += "<tr><td class='text-left'>OK</td><td class='text-right'>"+((ok/arr.length)*100).toFixed(2)+"%</td></tr>";
                ov += "<tr><td class='text-left'>Low</td><td class='text-right'>"+((low/arr.length)*100).toFixed(2)+"%</td></tr>";
                ov += "</table>";
                
                $("#dt").html(ov);
            }
            if(inp == "soc" && current_report != "soc"){
                for(var i = 0; i < markers.length; i++){ markers[i].setMap(null); }
                markers = [];
                for(var i = 0; i < polygons.length; i++){ polygons[i].setMap(null); }
                polygons = [];
                current_report = "soc";
                var ov = "<table style='width: 100%;'>";
                var soc = 0;
                for(var i = 0; i < arr.length; i++){
                    if(arr[i].SOC != null){
                        soc += parseFloat(arr[i].SOC);
                    }
                }
                ov += "<tr><td class='text-left'>Soil Organic Carbon Average Value</td><td class='text-right'>"+(soc/arr.length).toFixed(2)+"</td></tr>";
                ov += "</table>";
                
                ov += "<div id='chart-pie' class='text-left'></div><table style='width: 100%;'>";
                var high = 0;
                var ok = 0;
                var low = 0;
                hideMarkers();
                markers = [];
                for(var i = 0; i < arr.length; i++){
                    var color, label;
                    if(arr[i].SOC > 2){high++; color = "#fc5736"; label = "high";}
                    if(arr[i].SOC >= 1 && arr[i].SOC <= 2){ok++; color = "#57fc36"; label = "ok";}
                    if(arr[i].SOC < 1){low++; color = "#fcec36"; label = "low";}
                    
                    var tmp = arr[i].polygons.split("_");
                    var point = new google.maps.LatLng(tmp[0], tmp[1]);
                    if(google.maps.geometry.poly.containsLocation(point, farm)){
                        createMarkers(map, point, color, arr[i].SOC);
                    }
                }
                farm.setMap(map);
                $(document).ready(function(){
                    var chart = c3.generate({
                        bindto: '#chart-pie',
                        data: {
                            columns: [
                                ['high', high],
                                ['ok', ok],
                                ['low', low]
                            ],
                            type: 'pie',
                            colors: {
                                'high': "#fc5736",
                                'ok': "#57fc36",
                                'low': "#fcec36"
                            },
                            names: {
                                'high': 'High',
                                'ok': 'OK',
                                'low': 'Low'
                            }
                        },
                        axis: {
                        },
                        legend: {
                            show: true,
                        },
                        padding: {
                            bottom: 0,
                            top: 0
                        },
                    });
                });
                ov += "<tr><td class='text-left'>High</td><td class='text-right'>"+((high/arr.length)*100).toFixed(2)+"%</td></tr>";
                ov += "<tr><td class='text-left'>OK</td><td class='text-right'>"+((ok/arr.length)*100).toFixed(2)+"%</td></tr>";
                ov += "<tr><td class='text-left'>Low</td><td class='text-right'>"+((low/arr.length)*100).toFixed(2)+"%</td></tr>";
                ov += "</table>";
                
                $("#dt").html(ov);
            }
            if(inp == "ph" && current_report != "ph"){
                for(var i = 0; i < markers.length; i++){ markers[i].setMap(null); }
                markers = [];
                for(var i = 0; i < polygons.length; i++){ polygons[i].setMap(null); }
                polygons = [];
                current_report = "ph";
                var ov = "<table style='width: 100%;'>";
                var pH = 0;
                for(var i = 0; i < arr.length; i++){
                    if(arr[i].pH != null){
                        pH += parseFloat(arr[i].pH);
                    }
                }
                ov += "<tr><td class='text-left'>pH Average Value</td><td class='text-right'>"+(pH/arr.length).toFixed(2)+"</td></tr>";
                ov += "</table>";
                
                ov += "<div id='chart-pie' class='text-left'></div><table style='width: 100%;'>";
                var high = 0;
                var ok = 0;
                var low = 0;
                hideMarkers();
                markers = [];
                for(var i = 0; i < arr.length; i++){
                    var color, label;
                    if(arr[i].pH > 6.5){high++; color = "#fc5736"; label = "high";}
                    if(arr[i].pH >= 6 && arr[i].pH <= 6.5){ok++; color = "#57fc36"; label = "ok";}
                    if(arr[i].pH < 6){low++; color = "#fcec36"; label = "low";}
                    
                    var tmp = arr[i].polygons.split("_");
                    var point = new google.maps.LatLng(tmp[0], tmp[1]);
                    if(google.maps.geometry.poly.containsLocation(point, farm)){
                        createMarkers(map, point, color, arr[i].pH);
                    }
                }
                farm.setMap(map);
                $(document).ready(function(){
                    var chart = c3.generate({
                        bindto: '#chart-pie',
                        data: {
                            columns: [
                                ['high', high],
                                ['ok', ok],
                                ['low', low]
                            ],
                            type: 'pie',
                            colors: {
                                'high': "#fc5736",
                                'ok': "#57fc36",
                                'low': "#fcec36"
                            },
                            names: {
                                'high': 'High',
                                'ok': 'OK',
                                'low': 'Low'
                            }
                        },
                        axis: {
                        },
                        legend: {
                            show: true,
                        },
                        padding: {
                            bottom: 0,
                            top: 0
                        },
                    });
                });
                ov += "<tr><td class='text-left'>High</td><td class='text-right'>"+((high/arr.length)*100).toFixed(2)+"%</td></tr>";
                ov += "<tr><td class='text-left'>OK</td><td class='text-right'>"+((ok/arr.length)*100).toFixed(2)+"%</td></tr>";
                ov += "<tr><td class='text-left'>Low</td><td class='text-right'>"+((low/arr.length)*100).toFixed(2)+"%</td></tr>";
                ov += "</table>";
                
                $("#dt").html(ov);
            }
            if(inp == "moisture" && current_report != "moisture"){
                for(var i = 0; i < markers.length; i++){ markers[i].setMap(null); }
                markers = [];
                for(var i = 0; i < polygons.length; i++){ polygons[i].setMap(null); }
                polygons = [];
                current_report = "moisture";
                var ov = "<table style='width: 100%;'>";
                var moisture = 0;
                for(var i = 0; i < arr.length; i++){
                    if(arr[i].moisture != null){
                        moisture += parseFloat(arr[i].moisture);
                    }   
                }
                ov += "<tr><td class='text-left'>Moisture Average Value</td><td class='text-right'>"+(moisture/arr.length).toFixed(2)+"</td></tr>";
                ov += "</table>";
                
                ov += "<div id='chart-pie' class='text-left'></div><table style='width: 100%;'>";
                var high = 0;
                var ok = 0;
                var low = 0;
                hideMarkers();
                markers = [];
                for(var i = 0; i < arr.length; i++){
                    var color, label;
                    if(arr[i].moisture > 20){high++; color = "#fc5736"; label = "high";}
                    if(arr[i].moisture >= 10 && arr[i].moisture <= 20){ok++; color = "#57fc36"; label = "ok";}
                    if(arr[i].moisture < 10){low++; color = "#fcec36"; label = "low";}
                    
                    var tmp = arr[i].polygons.split("_");
                    var point = new google.maps.LatLng(tmp[0], tmp[1]);
                    if(google.maps.geometry.poly.containsLocation(point, farm)){
                        createMarkers(map, point, color, arr[i].moisture);
                    }
                }
                farm.setMap(map);
                $(document).ready(function(){
                    var chart = c3.generate({
                        bindto: '#chart-pie',
                        data: {
                            columns: [
                                ['high', high],
                                ['ok', ok],
                                ['low', low]
                            ],
                            type: 'pie',
                            colors: {
                                'high': "#fc5736",
                                'ok': "#57fc36",
                                'low': "#fcec36"
                            },
                            names: {
                                'high': 'High',
                                'ok': 'OK',
                                'low': 'Low'
                            }
                        },
                        axis: {
                        },
                        legend: {
                            show: true,
                        },
                        padding: {
                            bottom: 0,
                            top: 0
                        },
                    });
                });
                ov += "<tr><td class='text-left'>High</td><td class='text-right'>"+((high/arr.length)*100).toFixed(2)+"%</td></tr>";
                ov += "<tr><td class='text-left'>OK</td><td class='text-right'>"+((ok/arr.length)*100).toFixed(2)+"%</td></tr>";
                ov += "<tr><td class='text-left'>Low</td><td class='text-right'>"+((low/arr.length)*100).toFixed(2)+"%</td></tr>";
                ov += "</table>";
                
                $("#dt").html(ov);
            }
            if(inp == "p" && current_report != "p"){
                for(var i = 0; i < markers.length; i++){ markers[i].setMap(null); }
                markers = [];
                for(var i = 0; i < polygons.length; i++){ polygons[i].setMap(null); }
                polygons = [];
                var ov = "<table style='width: 100%;'>";
                var p = 0;
                for(var i = 0; i < arr.length; i++){
                    if(arr[i].P != null){
                        p += parseFloat(arr[i].P);
                    }
                }
                ov += "<tr><td class='text-left'>Phosphorous Average Value</td><td class='text-right'>"+(p/arr.length).toFixed(2)+"</td></tr>";
                ov += "</table>";
                
                ov += "<div id='chart-pie' class='text-left'></div><table style='width: 100%;'>";
                var high = 0;
                var ok = 0;
                var low = 0;
                hideMarkers();
                markers = [];
                for(var i = 0; i < arr.length; i++){
                    var color, label;
                    if(arr[i].P > 300){high++; color = "#fc5736"; label = "high";}
                    if(arr[i].P >= 100 && arr[i].P <= 300){ok++; color = "#57fc36"; label = "ok";}
                    if(arr[i].P < 100){low++; color = "#fcec36"; label = "low";}
                    
                    var tmp = arr[i].polygons.split("_");
                    var point = new google.maps.LatLng(tmp[0], tmp[1]);
                    if(google.maps.geometry.poly.containsLocation(point, farm)){
                        createMarkers(map, point, color, arr[i].P);
                    }
                }
                farm.setMap(map);
                $(document).ready(function(){
                    var chart = c3.generate({
                        bindto: '#chart-pie',
                        data: {
                            columns: [
                                ['high', high],
                                ['ok', ok],
                                ['low', low]
                            ],
                            type: 'pie',
                            colors: {
                                'high': "#fc5736",
                                'ok': "#57fc36",
                                'low': "#fcec36"
                            },
                            names: {
                                'high': 'High',
                                'ok': 'OK',
                                'low': 'Low'
                            }
                        },
                        axis: {
                        },
                        legend: {
                            show: true,
                        },
                        padding: {
                            bottom: 0,
                            top: 0
                        },
                    });
                });
                ov += "<tr><td class='text-left'>High</td><td class='text-right'>"+((high/arr.length)*100).toFixed(2)+"%</td></tr>";
                ov += "<tr><td class='text-left'>OK</td><td class='text-right'>"+((ok/arr.length)*100).toFixed(2)+"%</td></tr>";
                ov += "<tr><td class='text-left'>Low</td><td class='text-right'>"+((low/arr.length)*100).toFixed(2)+"%</td></tr>";
                ov += "</table>";
                
                $("#dt").html(ov);
            }
            if(inp == "k" && current_report != "k"){
                for(var i = 0; i < markers.length; i++){ markers[i].setMap(null); }
                markers = [];
                for(var i = 0; i < polygons.length; i++){ polygons[i].setMap(null); }
                polygons = [];
                var ov = "<table style='width: 100%;'>";
                var k = 0;
                for(var i = 0; i < arr.length; i++){
                    if(arr[i].K != null){
                        k += parseFloat(arr[i].K);
                    }
                }
                ov += "<tr><td class='text-left'>Potassium Average Value</td><td class='text-right'>"+(k/arr.length).toFixed(2)+"</td></tr>";
                ov += "</table>";
                
                ov += "<div id='chart-pie' class='text-left'></div><table style='width: 100%;'>";
                var high = 0;
                var ok = 0;
                var low = 0;
                hideMarkers();
                markers = [];
                for(var i = 0; i < arr.length; i++){
                    var color, label;
                    if(arr[i].K > 150){high++; color = "#fc5736"; label = "high";}
                    if(arr[i].K >= 100 && arr[i].K <= 150){ok++; color = "#57fc36"; label = "ok";}
                    if(arr[i].K < 100){low++; color = "#fcec36"; label = "low";}
                    
                    var tmp = arr[i].polygons.split("_");
                    var point = new google.maps.LatLng(tmp[0], tmp[1]);
                    if(google.maps.geometry.poly.containsLocation(point, farm)){
                        createMarkers(map, point, color, arr[i].K);
                    }
                }
                farm.setMap(map);
                $(document).ready(function(){
                    var chart = c3.generate({
                        bindto: '#chart-pie',
                        data: {
                            columns: [
                                ['high', high],
                                ['ok', ok],
                                ['low', low]
                            ],
                            type: 'pie',
                            colors: {
                                'high': "#fc5736",
                                'ok': "#57fc36",
                                'low': "#fcec36"
                            },
                            names: {
                                'high': 'High',
                                'ok': 'OK',
                                'low': 'Low'
                            }
                        },
                        axis: {
                        },
                        legend: {
                            show: true,
                        },
                        padding: {
                            bottom: 0,
                            top: 0
                        },
                    });
                });
                ov += "<tr><td class='text-left'>High</td><td class='text-right'>"+((high/arr.length)*100).toFixed(2)+"%</td></tr>";
                ov += "<tr><td class='text-left'>OK</td><td class='text-right'>"+((ok/arr.length)*100).toFixed(2)+"%</td></tr>";
                ov += "<tr><td class='text-left'>Low</td><td class='text-right'>"+((low/arr.length)*100).toFixed(2)+"%</td></tr>";
                ov += "</table>";
                
                $("#dt").html(ov);
            }
        }
        
        function setMapOnAll(map) {
            for (let i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }
        
        function hideMarkers() {
            setMapOnAll(null);
        }
        
    </script>
    <script src="{{asset('assets/bundles/c3.bundle.js')}}"></script>
    <script src="{{asset('assets/js/pages/charts/c3.js')}}"></script>
@stop
