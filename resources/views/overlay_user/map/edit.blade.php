@extends('user_layout.master')
@section('title', 'Fields')
@section('parentPageTitle', 'Map')
@section('page-style')
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.css') }}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyEYDW1fvNqG86HXoKuNsSxXJtlWtUG9k&v=3.exp&sensor=false&libraries=drawing,geometry,places&scroll=true"></script>
    <style>
        #map {
            height: 60vh;
            width: 100%;
            margin-bottom: 10px;
        }
    </style>
@stop
@section('content')
    <div class="row">
        <div class="col-md-6">
            <button class="btn btn-primary btn-lg" style="width: 100%;">The red area marks the old polygon. Please click to draw new polygon.</button>
        </div>
        <div class="col-md-6">
            <input id="pac-input" type="text" style="width: 100%; font-size: 18px; margin-top: 10px; border: 1px solid #ced4da; border-radius: 0.25rem; padding: 0.1rem 0.7rem;" placeholder="Search Box">
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-12">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            <button style="font-size: 18.5px; border: none; border-radius: 1.8px; background-color: #fff; margin-top: 5px;" id="delete-all-button"><i class="zmdi zmdi-delete"></i></button>
            <div id="map"></div>
            <form method="post" action="{{ route('field-update') }}" onsubmit="update_reports();">
                @csrf
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="field_id" placeholder="Field Name" value="{{ $data->id }}" />
                                <input type="text" class="form-control" name="field_name" placeholder="Field Name" value="{{ $data->field_name }}" required />
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="area" readonly name="acreage" placeholder="Acreage" value="{{ $data->acreage }}" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="crop_type" placeholder="Crop Type" value="{{ $data->crop_type }}" required />
                            </div>
                            <div class="form-group">
                                <select name="crop_stage" id="crop_stage" class="form-control" required>
                                    <option value="">Select Crop Stage</option>
                                    <option value="Mature" @if ($data->crop_stage == "Mature") {{'selected="selected"'}} @endif>Mature</option>
                                    <option value="Harvest" @if ($data->crop_stage == "Harvest") {{'selected="selected"'}} @endif>Harvest</option>
                                    <option value="Seeding" @if ($data->crop_stage == "Seeding") {{'selected="selected"'}} @endif>Seeding</option>
                                    <option value="Intermediate" @if ($data->crop_stage == "Intermediate") {{'selected="selected"'}} @endif>Intermediate</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-6">
                            <div class="form-group">
                            <select name="observer_id" data-live-search="true" class="form-control">
                                <option value="">Select Observer</option>
                                <?php
                                $result = DB::select("select * from users where id <> '" . Auth::user()->id . "'");
                                foreach($result as $r){
                                    echo "<option value='" . $r->id . "' ";
                                    if($data->observer_id == $r->id){echo 'selected="selected"';}
                                    echo " >" . $r->name . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        </div>
                        <div class="col-sm-3"></div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-sm-12 text-center">
                            <u>Reports</u><br />
                            <label><input type="checkbox" class="reports" value="1" <?php if(strpos($data->report, "1") !== false) { echo "checked"; } ?>> Plant Count</label>&nbsp;&nbsp;
                            <label><input type="checkbox" class="reports" value="2" <?php if(strpos($data->report, "2") !== false) { echo "checked"; } ?>> Nitrogen Level</label>&nbsp;&nbsp;
                            <label><input type="checkbox" class="reports" value="3" <?php if(strpos($data->report, "3") !== false) { echo "checked"; } ?>> Plant Stress</label>&nbsp;&nbsp;
                            <label><input type="checkbox" class="reports" value="4" <?php if(strpos($data->report, "4") !== false) { echo "checked"; } ?>> Water Stress</label>&nbsp;&nbsp;
                            <label><input type="checkbox" class="reports" value="5" <?php if(strpos($data->report, "5") !== false) { echo "checked"; } ?>> Plant Health</label>&nbsp;&nbsp;
                            <label><input type="checkbox" class="reports" value="6" <?php if(strpos($data->report, "6") !== false) { echo "checked"; } ?>> Canopy Cover</label>&nbsp;&nbsp;
                            <input type="hidden" name="report" id="report" value="" />
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="vertices" value="{{ $data->vertices }}" id="vertices" />
                                <input type="hidden" class="form-control" name="center" value="{{ $data->center }}" id="center" />
                                <button class="btn btn-primary"> Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('page-script')
    <script src="{{ asset('assets/plugins/momentjs/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/js/pages/forms/basic-form-elements.js') }}"></script>

    <script>
    
        function update_reports(){
            let text = "";
            var reps = $("input.reports:checked");
            for(var i = 0; i < reps.length; i++){
                text += reps[i]['value'] + ',';
            }
            document.getElementById("report").value = text;
        }
    
        var map; // Global declaration of the map
        var iw = new google.maps.InfoWindow(); // Global declaration of the infowindow
        var lat_longs = new Array();
        var markers = new Array();
        var drawingManager;
        var all_overlays = [];
        
        function initialize() {
            const coord = {{ $data->vertices }};
            var acreage = {{ $data->acreage }};
            
            var sma = 0;
            var smo = 0;
            for(var i = 0; i < coord.length; i++){
                smo += coord[i][0];
                sma += coord[i][1];
            }
            smo = smo / coord.length;
            sma = sma / coord.length;
            smo = smo.toFixed(14);
            sma = sma.toFixed(14);
            
            var starting_zoom = 21;
            if(acreage <= 2){ starting_zoom = 21; }
            if(acreage > 2 && acreage <= 30){ starting_zoom = 19; }
            if(acreage > 30 && acreage <= 60){ starting_zoom = 18; }
            if(acreage > 60 && acreage <= 100){ starting_zoom = 17; }
            if(acreage > 100){ starting_zoom = 16; }
            var bounds = new google.maps.LatLngBounds();
            var i;
            for (i = 0; i < coord.length; i++) {
                bounds.extend(coord[i]);
            }
            var ctr = bounds.getCenter();
            var myOptions = {
                zoom: starting_zoom,
                center: ctr,
                mapTypeId: google.maps.MapTypeId.SATELLITE,
                labels: true,
                zoomControl: true,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                    position: google.maps.ControlPosition.LEFT_BOTTOM,
                },
                scaleControl: false,
                streetViewControl: false,
                rotateControl: false,
                fullscreenControl: false
            }
            map = new google.maps.Map(document.getElementById("map"), myOptions);
            drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: google.maps.drawing.OverlayType.POLYGON,
                drawingControl: true,
                drawingControlOptions: {
                    drawingModes: [google.maps.drawing.OverlayType.POLYGON, google.maps.drawing.OverlayType.RECTANGLE, google.maps.drawing.OverlayType.CIRCLE],
                    position: google.maps.ControlPosition.TOP_CENTER
                },
                polygonOptions: {
                    editable: true
                }
            });
            drawingManager.setMap(map);
            
            const farm = new google.maps.Polygon({
                paths: coord,
                strokeColor: "#FF0000",
                strokeOpacity: 1,
                strokeWeight: 2,
                fillColor: "#FF0000",
                fillOpacity: 0,
            });
            farm.setMap(map);
            
            google.maps.event.addListener(drawingManager, "overlaycomplete", function(event) {
                all_overlays.push(event);
                overlayClickListener(event);
                if (event.type == "rectangle") {
                    var bounds = event.overlay.getBounds();
                    var sw = bounds.getSouthWest();
                    var ne = bounds.getNorthEast();
                    var a = [];
                    a.push(new google.maps.LatLng(sw.lat(), sw.lng()));
                    a.push(new google.maps.LatLng(sw.lat(), ne.lng()));
                    a.push(new google.maps.LatLng(ne.lat(), ne.lng()));
                    a.push(new google.maps.LatLng(ne.lat(), sw.lng()));
                    var pth = a.toString();
                    pth = pth.replaceAll("((", "(");
                    pth = pth.replaceAll("))", ")");
                }
                if (event.type == "circle") {
                    var center = event.overlay.getCenter();
                    var radius = event.overlay.getRadius();
                    var a = [], p = 360/360, d = 0;
                    for(var i = 0; i < 360; ++i, d += p) {
                        a.push(google.maps.geometry.spherical.computeOffset(center, radius, d));
                    }
                    var pth = a.toString();
                }
                if (event.type == "polygon") {
                    var a = event.overlay.getPath().getArray();
                    var pth = event.overlay.getPath().getArray().toString();
                }
                pth = pth.replaceAll("), ", ")|");
                pth = pth.replaceAll("(", "{lat:");
                pth = pth.replaceAll(", ", ",lng:");
                pth = pth.replaceAll(")", "}");
                pth = pth.replaceAll("}|", "},");
                pth = "[" + pth + "]";
                
                var area = google.maps.geometry.spherical.computeArea(a);
                area = area * 0.00024710538146717;
                var rounded_area = Math.round(area * 100) / 100;
                $('#area').val(rounded_area);
                
                var bounds = new google.maps.LatLngBounds();
                for (var i = 0; i < a.length; i++) {
                  bounds.extend(a[i]);
                }
                var pathCenter = bounds.getCenter().toString();
                pathCenter = pathCenter.replaceAll(")", "");
                pathCenter = pathCenter.replaceAll("(", "");
                $('#center').val(pathCenter);
                
                $('#vertices').val(pth);
                drawingManager.setDrawingMode(null);
            });
            
            function overlayClickListener(event) {
                setInterval(function() {
                    if (event.type == "polygon") {
                        var pth = event.overlay.getPath().getArray().toString();
                        pth = pth.replaceAll("), ", ")|");
                        pth = pth.replaceAll("(", "{lat:");
                        pth = pth.replaceAll(", ", ",lng:");
                        pth = pth.replaceAll(")", "}");
                        pth = pth.replaceAll("}|", "},");
                        pth = "[" + pth + "]";
                        $('#vertices').val(pth);
                        
                        var area = google.maps.geometry.spherical.computeArea(event.overlay.getPath());
                        area = area * 0.00024710538146717;
                        var rounded_area = Math.round(area * 100) / 100;
                        $('#area').val(rounded_area);
                        
                        var bounds = new google.maps.LatLngBounds();
                        var a = event.overlay.getPath().getArray();
                        for (var i = 0; i < a.length; i++) {
                          bounds.extend(a[i]);
                        }
                        var pathCenter = bounds.getCenter().toString();
                        pathCenter = pathCenter.replaceAll(")", "");
                        pathCenter = pathCenter.replaceAll("(", "");
                        $('#center').val(pathCenter);
                    }
                }, 2000);
            }
            
            var searchBox = new google.maps.places.SearchBox(document.getElementById('pac-input'));
            map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById('delete-all-button'));
            google.maps.event.addListener(searchBox, 'places_changed', function() {
                searchBox.set('map', null);
                var places = searchBox.getPlaces();
                var bounds = new google.maps.LatLngBounds();
                var i, place;
                for (i = 0; place = places[i]; i++) {
                    (function(place) {
                        var marker = new google.maps.Marker({
                            position: place.geometry.location
                        });
                        marker.bindTo('map', searchBox, 'map');
                        google.maps.event.addListener(marker, 'map_changed', function() {
                            if (!this.getMap()) {
                                this.unbindAll();
                            }
                        });
                        bounds.extend(place.geometry.location);
                    }(place));
                }
                map.fitBounds(bounds);
                searchBox.set('map', map);
                map.setZoom(Math.min(map.getZoom(),12));
            });
            
        }
        function deleteAllShape() {
            for (var i = 0; i < all_overlays.length; i++) {
                all_overlays[i].overlay.setMap(null);
            }
            all_overlays = [];
            $('#area').val("");
        }
        google.maps.event.addDomListener(window, 'load', initialize);
        google.maps.event.addDomListener(document.getElementById('delete-all-button'), 'click', deleteAllShape);
        $('.reports').on('change', function() {
            update_reports();
        });
    </script>
@stop
