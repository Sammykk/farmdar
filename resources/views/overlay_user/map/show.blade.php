@extends('user_layout.master')
@section('title', 'Fields')
@section('parentPageTitle', 'Map')
@section('page-style')
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.css') }}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyEYDW1fvNqG86HXoKuNsSxXJtlWtUG9k&callbackv=3.exp&sensor=false&libraries=drawing,geometry,places&scroll=true"></script>
    <style>
        #color-palette {
            clear: both;
        }
        .color-button {
            width: 14px;
            height: 14px;
            font-size: 0;
            margin: 2px;
            float: left;
            cursor: pointer;
        }
        #map {
            height: 60vh;
            width: 100%;
            margin-bottom: 10px;
        }
    </style>
@stop
@section('content')
    <?php if($data->observer_id != Auth::user()->id){ ?>
    <div class="row clearfix">
        <div class="col-md-12 text-center">
            <form method="post" action="{{ route('update-report') }}" onsubmit="update_reports();">
                @csrf
                <div class="row clearfix">
                    <div class="col-md-9">
                        <u>Reports:</u>&nbsp;&nbsp;&nbsp;
                        <label><input type="checkbox" class="reports" value="1" <?php if(strpos($data->report, "1") !== false) { echo "checked"; } ?>> Plant Count</label>&nbsp;&nbsp;
                        <label><input type="checkbox" class="reports" value="2" <?php if(strpos($data->report, "2") !== false) { echo "checked"; } ?>> Nitrogen Level</label>&nbsp;&nbsp;
                        <label><input type="checkbox" class="reports" value="3" <?php if(strpos($data->report, "3") !== false) { echo "checked"; } ?>> Plant Stress</label>&nbsp;&nbsp;
                        <label><input type="checkbox" class="reports" value="4" <?php if(strpos($data->report, "4") !== false) { echo "checked"; } ?>> Water Stress</label>&nbsp;&nbsp;
                        <label><input type="checkbox" class="reports" value="5" <?php if(strpos($data->report, "5") !== false) { echo "checked"; } ?>> Plant Health</label>&nbsp;&nbsp;
                        <label><input type="checkbox" class="reports" value="6" <?php if(strpos($data->report, "6") !== false) { echo "checked"; } ?>> Canopy Cover</label>&nbsp;&nbsp;
                        <input type="hidden" name="report" id="report" value="" />
                        <input type="hidden" name="get_id" id="get_id" value="{{ $data->id }}" />
                        <br />
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <select name="observer_id" data-live-search="true" class="form-control">
                                <option value="">Select Observer</option>
                                <?php
                                $result = DB::select("select * from users where id <> '" . Auth::user()->id . "'");
                                foreach($result as $r){
                                    echo "<option value='" . $r->id . "' ";
                                    if($data->observer_id == $r->id){echo 'selected="selected"';}
                                    echo " >" . $r->name . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php } ?>
    <div class="row clearfix">
        <div class="col-md-10">
            <div class="card">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
                <div id="map"></div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="card">
                <div class="header">
                    <h2><strong>Field Parameters</strong></h2><br />
                    <b>Record ID:<br /></b>{{ $data->id }}<br /><br />
                    <b>Field Name:<br /></b>{{ $data->field_name }}<br /><br />
                    <b>Acereage Area:<br /></b>{{ round($data->acreage, 2) }}<br /><br />
                    <?php
                    $rep = ["", "Plant Count", "Nitrogen Level", "Plant Stress", "Water Stress", "Plant Health", "Canopy Cover"];
                    $itr = explode(",", $data->report);
                    $otr = "";
                    for($i = 0; $i < count($itr) - 1; $i++){
                        $otr .= "<span>" . $rep[$itr[$i]] . "<br /></span>";
                    }
                    ?>
                    <b>Report:<br /></b><?php echo $otr; ?><br />
                    <b>Crop Type:<br /></b>{{ $data->crop_type }}<br /><br />
                    <b>Crop Stage:<br /></b>{{ $data->crop_stage }}<br /><br />
                    <!--<b>Farm Vertices:<br /></b>{{ $data->vertices }}<br /><br />-->
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-script')

    <script src="{{ asset('assets/plugins/momentjs/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/js/pages/forms/basic-form-elements.js') }}"></script>

    <script>
        
        $(document).on('keyup', '.bootstrap-select .dropdown-menu .bs-searchbox input', function() {
            $(this).parent().parent().parent().css('z-index', 1060);
        });
    
        function update_reports(){
            let text = "";
            var reps = $("input.reports:checked");
            for(var i = 0; i < reps.length; i++){
                text += reps[i]['value'] + ',';
            }
            document.getElementById("report").value = text;
        }
    
        function initialize() {
            const coord = {{ $data->vertices }};
            var acreage = {{ $data->acreage }};
            
            var sma = 0;
            var smo = 0;
            for(var i = 0; i < coord.length; i++){
                smo += coord[i][0];
                sma += coord[i][1];
            }
            smo = smo / coord.length;
            sma = sma / coord.length;
            smo = smo.toFixed(14);
            sma = sma.toFixed(14);
            
            var starting_zoom = 20;
            if(acreage <= 2){ starting_zoom = 20; }
            if(acreage > 2 && acreage <= 30){ starting_zoom = 17; }
            if(acreage > 30 && acreage <= 60){ starting_zoom = 16; }
            if(acreage > 60 && acreage <= 100){ starting_zoom = 15; }
            if(acreage > 100){ starting_zoom = 14; }
            var bounds = new google.maps.LatLngBounds();
            var i;
            for (i = 0; i < coord.length; i++) {
                bounds.extend(coord[i]);
            }
            var ctr = bounds.getCenter();
            const map = new google.maps.Map(document.getElementById("map"), {
                zoom: starting_zoom,
                center: ctr,
                mapTypeId: google.maps.MapTypeId.SATELLITE,
                labels: true
            });
            
            const farm = new google.maps.Polygon({
                paths: coord,
                strokeColor: "#FF0000",
                strokeOpacity: 1,
                strokeWeight: 2,
                fillColor: "#FF0000",
                fillOpacity: 0,
            });
            farm.setMap(map);
        }

        
        $('.ls-toggle-btn').on('click', function() {
            setTimeout(function() {
                map.resize();
            }, 500);
        });
        $('.reports').on('change', function() {
            update_reports();
        });
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
@stop
