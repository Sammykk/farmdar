@extends('user_layout.master')
@section('title', 'Fields')
@section('parentPageTitle', 'Map')
@section('page-style')
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.css') }}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyEYDW1fvNqG86HXoKuNsSxXJtlWtUG9k&v=3.exp&sensor=false&libraries=drawing,geometry,places&scroll=true"></script>
    <style>
        #map-canvas {
            height: 60vh;
            width: 100%;
            margin-bottom: 10px;
        }
    </style>
@stop
@section('content')
    <div class="row">
        <div class="col-md-6">
            <button class="btn btn-primary btn-lg" style="width: 100%;">Find your fields by typing the location in the search bar.</button>
        </div>
        <div class="col-md-6">
            <input id="pac-input" type="text" style="width: 100%; font-size: 18px; margin-top: 10px; border: 1px solid #ced4da; border-radius: 0.25rem; padding: 0.1rem 0.7rem;" placeholder="Search Box">
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-md-12">
            <button style="font-size: 18.5px; border: none; border-radius: 1.8px; background-color: #fff; margin-top: 5px;" id="delete-all-button"><i class="zmdi zmdi-delete"></i></button>
            <div id="map-canvas"></div>
            <form method="post" accept-charset="utf-8" id="map_form" action="{{ route('fiels-store') }}">
                @csrf
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="field_name" placeholder="Field Name" required />
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="area" readonly name="acreage" placeholder="Acreage" value="" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="crop_type" placeholder="Crop Type" required />
                            </div>
                            <div class="form-group">
                                <select name="crop_stage" id="crop_stage" class="form-control" required>
                                    <option value="">Select Crop Stage</option>
                                    <option value="Mature">Mature</option>
                                    <option value="Harvest">Harvest</option>
                                    <option value="Seeding">Seeding</option>
                                    <option value="Intermediate">Intermediate</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="vertices" value="" id="vertices" />
                                <input type="hidden" class="form-control" name="center" value="" id="center" />
                                <button class="btn btn-primary"> Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('page-script')
    <script src="{{ asset('assets/plugins/momentjs/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/js/pages/forms/basic-form-elements.js') }}"></script>

    <script>
        var map; // Global declaration of the map
        var iw = new google.maps.InfoWindow(); // Global declaration of the infowindow
        var lat_longs = new Array();
        var markers = new Array();
        var drawingManager;
        var all_overlays = [];
        
        function initialize() {
            var myLatlng = new google.maps.LatLng(31.976515, 74.222015);
            var myOptions = {
                zoom: 5,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.SATELLITE,
                labels: true,
                zoomControl: true,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                    position: google.maps.ControlPosition.LEFT_BOTTOM,
                },
                scaleControl: false,
                streetViewControl: false,
                rotateControl: false,
                fullscreenControl: false
            }
            map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
            drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: google.maps.drawing.OverlayType.POLYGON,
                drawingControl: true,
                drawingControlOptions: {
                    drawingModes: [google.maps.drawing.OverlayType.POLYGON, google.maps.drawing.OverlayType.RECTANGLE, google.maps.drawing.OverlayType.CIRCLE],
                    position: google.maps.ControlPosition.TOP_CENTER
                },
                polygonOptions: {
                    editable: true
                }
            });
            drawingManager.setMap(map);
            
            google.maps.event.addListener(drawingManager, "overlaycomplete", function(event) {
                all_overlays.push(event);
                overlayClickListener(event);
                if (event.type == "rectangle") {
                    var bounds = event.overlay.getBounds();
                    var sw = bounds.getSouthWest();
                    var ne = bounds.getNorthEast();
                    var a = [];
                    a.push(new google.maps.LatLng(sw.lat(), sw.lng()));
                    a.push(new google.maps.LatLng(sw.lat(), ne.lng()));
                    a.push(new google.maps.LatLng(ne.lat(), ne.lng()));
                    a.push(new google.maps.LatLng(ne.lat(), sw.lng()));
                    var pth = a.toString();
                    pth = pth.replaceAll("((", "(");
                    pth = pth.replaceAll("))", ")");
                }
                if (event.type == "circle") {
                    var center = event.overlay.getCenter();
                    var radius = event.overlay.getRadius();
                    var a = [], p = 360/360, d = 0;
                    for(var i = 0; i < 360; ++i, d += p) {
                        a.push(google.maps.geometry.spherical.computeOffset(center, radius, d));
                    }
                    var pth = a.toString();
                }
                if (event.type == "polygon") {
                    var a = event.overlay.getPath().getArray();
                    var pth = event.overlay.getPath().getArray().toString();
                }
                pth = pth.replaceAll("), ", ")|");
                pth = pth.replaceAll("(", "{lat:");
                pth = pth.replaceAll(", ", ",lng:");
                pth = pth.replaceAll(")", "}");
                pth = pth.replaceAll("}|", "},");
                pth = "[" + pth + "]";
                
                var area = google.maps.geometry.spherical.computeArea(a);
                area = area * 0.00024710538146717;
                var rounded_area = Math.round(area * 100) / 100;
                $('#area').val(rounded_area);
                
                var bounds = new google.maps.LatLngBounds();
                for (var i = 0; i < a.length; i++) {
                  bounds.extend(a[i]);
                }
                var pathCenter = bounds.getCenter().toString();
                pathCenter = pathCenter.replaceAll(")", "");
                pathCenter = pathCenter.replaceAll("(", "");
                $('#center').val(pathCenter);
                
                $('#vertices').val(pth);
                drawingManager.setDrawingMode(null);
            });
            
            function overlayClickListener(event) {
                setInterval(function() {
                    if (event.type == "polygon") {
                        var pth = event.overlay.getPath().getArray().toString();
                        pth = pth.replaceAll("), ", ")|");
                        pth = pth.replaceAll("(", "{lat:");
                        pth = pth.replaceAll(", ", ",lng:");
                        pth = pth.replaceAll(")", "}");
                        pth = pth.replaceAll("}|", "},");
                        pth = "[" + pth + "]";
                        $('#vertices').val(pth);
                        
                        var area = google.maps.geometry.spherical.computeArea(event.overlay.getPath());
                        area = area * 0.00024710538146717;
                        var rounded_area = Math.round(area * 100) / 100;
                        $('#area').val(rounded_area);
                        
                        var bounds = new google.maps.LatLngBounds();
                        var a = event.overlay.getPath().getArray();
                        for (var i = 0; i < a.length; i++) {
                          bounds.extend(a[i]);
                        }
                        var pathCenter = bounds.getCenter().toString();
                        pathCenter = pathCenter.replaceAll(")", "");
                        pathCenter = pathCenter.replaceAll("(", "");
                        $('#center').val(pathCenter);
                    }
                }, 2000);
            }
            
            var searchBox = new google.maps.places.SearchBox(document.getElementById('pac-input'));
            map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById('delete-all-button'));
            google.maps.event.addListener(searchBox, 'places_changed', function() {
                searchBox.set('map', null);
                var places = searchBox.getPlaces();
                var bounds = new google.maps.LatLngBounds();
                var i, place;
                for (i = 0; place = places[i]; i++) {
                    (function(place) {
                        var marker = new google.maps.Marker({
                            position: place.geometry.location
                        });
                        marker.bindTo('map', searchBox, 'map');
                        google.maps.event.addListener(marker, 'map_changed', function() {
                            if (!this.getMap()) {
                                this.unbindAll();
                            }
                        });
                        bounds.extend(place.geometry.location);
                    }(place));
                }
                map.fitBounds(bounds);
                searchBox.set('map', map);
                map.setZoom(Math.min(map.getZoom(),12));
            });
        }
        
        function deleteAllShape() {
            for (var i = 0; i < all_overlays.length; i++) {
                all_overlays[i].overlay.setMap(null);
            }
            all_overlays = [];
            $('#area').val("");
        }
        google.maps.event.addDomListener(document.getElementById('delete-all-button'), 'click', deleteAllShape);
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
@stop
