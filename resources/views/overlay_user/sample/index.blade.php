@extends('user_layout.master')
@section('title', 'Sample Report')
@section('parentPageTitle', 'Sample Report')
@section('page-style')
    <link rel="stylesheet" href="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css') }}" />
@stop
@section('content')
<div class="row">
    <div class="col-log-8">
        <div class="alert alert-primary" role="alert">
            <div class="container">
                <p class="info" id="info">Thank you for interest in FARMDAR. If you have any question,<a href="https://farmdar.co.uk/contact/">Click Here</a> to fill out our contact form and sales representative will contact you</p>
                <p class="info" id="infos"></p>
            </div>
        </div>
    </div>
</div>


    <div class="row clearfix">
        <div class="col-md-12 col-lg-12">
            <div class="card" >
                
               <h5>Sample Report</h5>
              
               <embed src="{{asset('assets/images/file.pdf')}}" width="100%" height="2100px" />
            </div>
        </div>

    </div>
@stop

@section('page-script')
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0"></script>
    <script src="{{ asset('assets/bundles/jvectormap.bundle.js') }}"></script>
    <script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-in-mill.js') }}"></script>
    <script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-us-aea-en.js') }}"></script>
    <script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-uk-mill-en.js') }}"></script>
    <script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-au-mill.js') }}"></script>
    <script src="{{ asset('assets/js/pages/maps/jvectormap.js') }}"></script>




@stop
