@extends('user_layout.master')
@section('title', 'Report')
@section('parentPageTitle', 'Map')
@section('page-style')
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.css') }}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
@stop
@section('content')
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2><strong>Report Parameters</strong></h2>
                    <b>Group Id:</b> {{ $data->group_id }}<br/>
                    <b>Report Type:</b> {{$data->report_type}}<br />
                    <b>Device Id:</b> {{ $data->device_id }}<br/>
                    <b>Useful Value:</b> {{ $data->paddockNames }}<br/>
                    <b>Lat:</b> {{ $data->lat }}<br/>
                    <b>Lng:</b> {{ $data->lng }}<br/>
                    <b>Polygons:</b> {{ $data->polygons }}<br/>
                    <b>Model:</b> {{ $data->model }}<br/>
                    <b>Nmin:</b> {{ $data->Nmin }}<br/>
                    <b>Soil Organic Carbon:</b> {{ $data->SOC }}<br/>
                    <b>pH:</b> {{ $data->pH }}<br/>
                    <b>Phosphorous:</b> {{ $data->P }}<br/>
                    <b>Potassium:</b> {{ $data->K }}<br/>
                    <b>Created At:</b> {{ $data->created_at }}<br/>
                    <b>Updated At:</b> {{ $data->updated_at }}<br/>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page-script')
    <script src="{{ asset('assets/plugins/momentjs/moment.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/js/pages/forms/basic-form-elements.js') }}"></script>
@stop
