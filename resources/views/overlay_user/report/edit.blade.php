@extends('user_layout.master')
@section('title', 'Report')
@section('parentPageTitle', 'Map')
@section('page-style')
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.css') }}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyEYDW1fvNqG86HXoKuNsSxXJtlWtUG9k&v=3.exp&sensor=false&libraries=drawing,geometry,places&scroll=true"></script>
    <style>
        #map {
            height: 60vh;
            width: 100%;
            margin-bottom: 10px;
        }
        .form-control{
            margin-bottom: 5px;
        }
    </style>
@stop
@section('content')
    <div class="row clearfix">
        <div class="col-md-12">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            <form method="post" action="{{ route('update-report') }}">
                @csrf
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <b>Report Id:</b> <input type="text" class="form-control" name="id" readonly value="{{ $data->id }}" />
                                <b>Group Id:</b> <input type="text" class="form-control" name="group_id" placeholder="Group ID" value="{{ $data->group_id }}" />
                                <b>Report Type:</b> <input type="text" class="form-control" name="report_type" placeholder="Report Type" value="{{ $data->report_type }}" />
                                <b>Device Id:</b> <input type="text" class="form-control" name="device_id" placeholder="Device ID" value="{{ $data->device_id }}" />
                                <b>Useful Value:</b> <input type="text" class="form-control" name="paddockNames" placeholder="Useful Value" value="{{ $data->paddockNames }}" />
                                <b>Lat:</b> <input type="text" class="form-control" name="lat" placeholder="Latitude" value="{{ $data->lat }}" />
                                <b>Lng:</b> <input type="text" class="form-control" name="lng" placeholder="Longitude" value="{{ $data->lng }}" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <b>Polygons:</b> <input type="text" class="form-control" name="polygons" placeholder="Polygons" value="{{ $data->polygons }}" />
                                <b>Model:</b> <input type="text" class="form-control" name="model" placeholder="Model" value="{{ $data->model }}" />
                                <b>Nmin:</b> <input type="text" class="form-control" name="Nmin" placeholder="Nmin" value="{{ $data->Nmin }}" />
                                <b>Soil Organic Carbon:</b> <input type="text" class="form-control" name="SOC" placeholder="Soil Organic Carbon" value="{{ $data->SOC }}" />
                                <b>pH:</b> <input type="text" class="form-control" name="pH" placeholder="pH" value="{{ $data->pH }}" />
                                <b>Phosphorous:</b> <input type="text" class="form-control" name="P" placeholder="Phosphorous" value="{{ $data->P }}" />
                                <b>Potassium:</b> <input type="text" class="form-control" name="K" placeholder="Potassium" value="{{ $data->K }}" />
                            </div>
                        </div>
                        <div class="col-sm-12 text-center">
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Submit" />
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop
