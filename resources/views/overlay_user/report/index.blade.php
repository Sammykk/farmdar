@extends('user_layout.master')
@section('title', 'Reports')
@section('parentPageTitle', 'Tables')
@section('page-style')
<link rel="stylesheet" href="{{asset('assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css')}}"/>
<style>
    .dropdown-toggle{ display: none; }
    .dropdown-menu{ display: none; }
    #report_type{
        padding: 4px;
        border-radius: 3px;
    }
</style>
@stop
@section('content')

<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12">
        <div class="card">
            <div class="body">
                <div class="col-md-12 text-center">
                     @if(session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <form method="post" action="{{ route('create-report') }}" enctype="multipart/form-data">
                        @csrf
                        <select name="report_type" id="report_type">
                            <option value="">Report Type</option>
                            <option value="population">Population</option>
                            <option value="canopy">Canopy</option>
                            <option value="soil">Soil</option>
                            <option value="health">Health</option>
                            <option value="nitrogen_requirement">Nitrogen Requirement</option>
                            <option value="plant_stress">Plant Stress</option>
                            <option value="water_stress">Water Stress</option>
                        </select>
                        <b>Select Report File:</b>&nbsp;&nbsp;
                        <input type="file" placeholder="Upload Report" name="report_file" required />
                        <input type="submit" class="btn btn-primary" value="Submit Report" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('page-script')
<script src="{{asset('assets/bundles/datatablescripts.bundle.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/js/pages/tables/jquery-datatable.js')}}"></script>
@stop
