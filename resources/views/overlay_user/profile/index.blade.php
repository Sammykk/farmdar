@extends('user_layout.master')
@section('title', 'Profile')
@section('parentPageTitle', 'My Profile')
@section('content')
<div class="row clearfix">
    <div class="col-lg-12">
        <div class="card" >
            <form method="post" accept-charset="utf-8" action="{{route('user.profileedit')}}">
                @csrf
                <a href="javascript:void(0);"><img src="{{asset('assets/images/profile_av.jpg')}}" class="rounded-circle" alt="profile-image"></a>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="Name" value="{{$data->name}}"/>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="email" placeholder="Email" readonly value="{{$data->email}}" />
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="number" placeholder="Phone Number" value="{{$data->number}}" />
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="address" placeholder="Address" value="{{$data->address}}"/>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control"  name="password" placeholder="8 letter password"  />
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary"> Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop