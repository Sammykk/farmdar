@extends('user_layout.master')
@section('title', 'Account')
@section('parentPageTitle', 'Account')
@section('page-style')
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.css') }}" />
@stop
@section('content')
<div class="row clearfix">
    <div class="col-lg-12">
        <div class="card" >
            <form method="post" accept-charset="utf-8" action="{{route('user.store_account')}}">
                @csrf
                <a href="javascript:void(0);"><img src="{{asset('assets/images/profile_av.jpg')}}" class="rounded-circle" alt="profile-image"></a>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="Name" required />
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="email" placeholder="Email" required />
                            </div>
                            <div class="form-group">
                                <select name="role_id" class="form-control" required>
                                    <option value="" selected>Select Role</option>
                                    <option value="2">User</option>
                                    <option value="1">Admin</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <input type="text" class="form-control" name="address" placeholder="Address" />
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="number" placeholder="Phone Number" />
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="8 letter password" required />
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Create" />
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop