@extends('user_layout.master')
@section('title', 'Observed fields')
@section('parentPageTitle', 'Tables')
@section('page-style')
<link rel="stylesheet" href="{{asset('assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css')}}"/>
@stop
@section('content')

<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12">
        <div class="card">

            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Crop Stage</th>
                                <th>Crop Type</th>
                                <th>Date</th>
                                <th>Acreage</th>
                                <th>Creator</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($fields as $field)
                            <?php
                            $user_id = Auth::user()->id;
                            if($field->observer_id == $user_id){
                                ?>
                                <tr>
                                    {{-- <td><img src="H.gif" alt="" border=3 ></img></td> --}}
                                    <td> <a href="javascript:void(0);"><img src="{{asset('assets/images/map.png')}}" class="rounded-circle" alt="map-image" height=70 width=70></a></td>
                                    <td><a href="{{ url('user/fields/show/'.$field->id)}}">{{$field->field_name}}</a></td>
                                    <td>{{$field->crop_stage}}</td>
                                    <td>{{$field->crop_type}}</td>
                                    <td>{{$field->updated_at}}</td>
                                    <td>{{$field->acreage}}</td>
                                    <td><?php
                                    $result = DB::select("select * from users where id = '" . $field->create . "'");
                                    foreach($result as $r){
                                        echo $r->name;
                                        break;
                                    }
                                    ?></td>
                                    <td class="icon">
                                        <!--<i class="zmdi zmdi-hc-fw"></i><a href="{{ url('user/fields/edit/'.$field->id)}}">Edit</a>-->
                                        <!--<i class="zmdi zmdi-hc-fw"></i><a href="">Delete</a>-->
                                        <i class="zmdi zmdi-chart"></i> <a href="{{ url('user/fields/display_report/'.$field->id)}}">Report</a>
                                    </td>
                                </tr>
                                <?php
                            }                            
                            ?>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('page-script')
<script src="{{asset('assets/bundles/datatablescripts.bundle.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/js/pages/tables/jquery-datatable.js')}}"></script>
@stop
