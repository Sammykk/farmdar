@extends('user_layout.master')
@section('title', 'Fields')
@section('parentPageTitle', 'Tables')
@section('page-style')
<link rel="stylesheet" href="{{asset('assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css')}}"/>
@stop
@section('content')

<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="card">
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>Field ID</th>
                                <th>Name</th>
                                <th>Created By</th>
                                <th>Crop Stage</th>
                                <th>Crop Type</th>
                                <th>Updated At</th>
                                <th>Acreage</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($fields as $field)
                            <?php
                                $result = DB::select("select * from users where id = '" . $field->create . "'");
                                foreach($result as $r){
                                    $creator = $r->name;
                                    break;
                                }
                            ?>
                            <tr>
                                <td>{{$field->id}}</td>
                                <td><a href="{{ url('user/fields/process/'.$field->id)}}" title="Process">{{$field->field_name}}</a></td>
                                <td>{{$creator}}</td>
                                <td>{{$field->crop_stage}}</td>
                                <td>{{$field->crop_type}}</td>
                                <td>{{$field->updated_at}}</td>
                                <td>{{$field->acreage}}</td>
                                <td class="icon text-center">
                                    <!--<a href="{{ url('user/fields/show/'.$field->id)}}" title="View"><i class="zmdi zmdi-eye"></i></a>&nbsp;&nbsp;-->
                                    <a href="{{ url('user/fields/remove/'.$field->id)}}" title="Delete" onclick="return confirm('Are you sure?')"><i class="zmdi zmdi-delete"></i></a>&nbsp;&nbsp;
                                    <a href="{{ url('user/fields/display_report/'.$field->id)}}" title="Report"><i class="zmdi zmdi-chart"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('page-script')
<script src="{{asset('assets/bundles/datatablescripts.bundle.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-datatable/buttons/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/js/pages/tables/jquery-datatable.js')}}"></script>
@stop
