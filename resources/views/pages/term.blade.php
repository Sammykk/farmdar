<html>
<head>
    <title>Terms And Conditions</title>
</head>
<body>
@section('sidebar')
    <h2>Terms And Conditions</h2>
    @foreach($terms as $term)
        <p>{{$term->term}}</p>
    @endforeach
@show

<div class="container">
    @yield('content')
</div>
</body>
</html>