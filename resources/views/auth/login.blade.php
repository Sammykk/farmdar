<style>
    body{
        background-image: url('{{ url('/assets/images/login-bg.jpg') }}')
    }
</style>
@extends('layout.authentication')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-4 col-sm-12">
            <div class="card">
                <!-- <div class="card-header">{{ __('Login') }}</div> -->

                <form method="POST" action="{{ route('login') }}">
                   @csrf


                    <div class="header text-center">
                        <img class="logo" src="{{ url('/assets/images/logopdf2.png') }}" alt="">
                        <h5>Log in</h5>
                    </div>
                    <div class="card-body">

                        <div class="input-group mb-3">

                            <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-email"></i></span>
                            </div>
                            @if ($errors->has('email'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif

                        </div>

                        <div class="input-group mb-3">

                            <input id="password" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-lock"></i></span>
                            </div>
                            @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif

                        </div>

                        <div class="input-group mb-3">

                            <div class="">
                                <label>
                                    <input type="checkbox" name="remember"> {{ __('Remember Me') }}
                                </label>
                            </div>

                        </div>



                        <button type="submit" class="btn btn-primary btn-block waves-effect waves-light">
                            {{ __('Login') }}
                        </button>
                        <a href="{{ route('register') }}" class="btn btn-primary btn-block waves-effect waves-light">
                            {{ __('Register') }}
                        </a>
                        {{-- <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('auth.forgot') }}
                        </a> --}}





                    </div>

                </form>

            </div>
        </div>
        <div class="col-lg-2 col-sm-12">

        </div>
        <div class="col-lg-6 col-sm-12">
            <div class="card">
                <img src="{{ asset('assets/images/logopdf2.png') }}" alt="Sign In" />
            </div>
        </div>
    </div>
</div>
@endsection
