@extends('layout.authentication')
<script src="http://code.jquery.com/jquery-3.6.0.min.js"></script>
 

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-4 col-sm-12">
            <div class="header text-center">
                <img class="logo" src="{{ url('/assets/images/logopdf2.png') }}" alt="">
                <h5>Sign Up</h5>

            </div>
            <div class="body">

                    <form id="register_form" class="card auth_form" method="POST" action="{{ route('register') }}">
                        @csrf

                      
                        <div id="form1">
                        <div class="input-group mb-3 ">



                            <input required id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}"    placeholder="Type Name">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-account-circle"></i></span>
                            </div>

                            @if ($errors->has('name'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                            @endif

                        </div>

                        <div class="input-group mb-3 ">



                            <input required id="surname" type="text" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" value="{{ old('name') }}"  placeholder="Type Surname">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-account-circle"></i></span>
                            </div>

                            @if ($errors->has('surname'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('surname') }}</strong>
                            </span>
                            @endif

                            </div>


                        <div class="input-group mb-3 ">


                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required  placeholder="Type Email">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-email"></i></span>
                            </div>
                            @if ($errors->has('email'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif

                        </div>


                        <div class="input-group mb-3 ">


                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required  placeholder="Type Password">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-lock"></i></span>
                            </div>
                            @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif

                        </div>

                        <div class="input-group mb-3 ">

                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Re-Type Password">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-lock"></i></span>
                            </div>

                        </div>

                        </div>

                        <div id="form2"  style="display:none;">
                          <div class="input-group mb-3 ">


                            <select class="form-control" name="user_type">
                                @foreach($user_type as $value)
                                    <option value="{{$value['id']}}">{{$value['type']}}</option>
                                @endforeach
                            </select>
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-account-circle"></i></span>
                            </div>
                            @if ($errors->has('user_type'))
                                <span class="invalid-feedback">
                                <strong>{{ $errors->first('user_type') }}</strong>
                            </span>
                            @endif

                            </div>

                        

                          <div class="input-group mb-3 ">

                        <input required id="nic" type="text" class="form-control{{ $errors->has('nic') ? ' is-invalid' : '' }}" name="nic" value="{{ old('name') }}"  placeholder="Type NIC Number">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="zmdi zmdi-account-circle"></i></span>
                        </div>

                        @if ($errors->has('nic'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('nic') }}</strong>
                        </span>
                        @endif

                    </div>


                        <div class="input-group mb-3 ">

                            <input  id="number" type="text" class="form-control{{ $errors->has('number') ? ' is-invalid' : '' }}" name="number" value="{{ old('name') }}"  placeholder="Type Mobile Number">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="zmdi zmdi-account-circle"></i></span>
                            </div>

                            @if ($errors->has('number'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('number') }}</strong>
                            </span>
                            @endif

                        </div>


                        <div class="input-group mb-3 ">

                                <input id="kcard" type="text" class="form-control{{ $errors->has('kcard') ? ' is-invalid' : '' }}" name="kcard" value="{{ old('name') }}" required autofocus  placeholder="Type Kisan Card Number">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="zmdi zmdi-account-circle"></i></span>
                                </div>

                                @if ($errors->has('kcard'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('kcard') }}</strong>
                                </span>
                                @endif

                            </div>

                            <div class="input-group mb-3 ">
                                <input id="terms" type="checkbox" class="form-control" name="terms" value="1" required/> <a href="{{route('term.show')}}">Agree with the terms and conditions</a>
                            </div>


                            </div>


                        <div id="next"  style="display:block;" class="input-group mb-3 ">

                            <button type="button" class="btn btn-primary btn-block waves-effect waves-light">
                               Next
                            </button>

                        </div>

                        <div id="submit"  style="display:none;" class="input-group mb-3 ">

                            <button type="submit" class="btn btn-primary btn-block waves-effect waves-light">
                                {{ __('Register') }}
                            </button>

                        </div>



                    </form>
                    <a href="{{ route('login') }}" class="btn btn-primary btn-block waves-effect waves-light">Already have
                        account, Login here!</a>


            </div>
        </div>
        <div class="col-lg-2 col-sm-12">
        </div>
        <div class="col-lg-6 col-sm-12">
            <div class="card">
                <img src="{{ asset('assets/images/logopdf2.png') }}" alt="Sign Up" />
            </div>
        </div>
    </div>
</div>
<script>
 $(document).ready(function(){

   
    $("#name,#surname,  #email, #password, #passwordc").click(function(){
            $(this).css("border", "1px solid #ced4da");
    });

    $("#next").click(function(){


        var name = $("#name").val();
        var surname = $("#surname").val();

        var email = $("#email").val();
        var password = $("#password").val();
        var passwordc = $("#password-confirm").val();

        if($.trim(name)  == ""){
            $("#name").css("border", "2px solid red");
        }
       else if($.trim(surname)  == ""){
            $("#surname").css("border", "2px solid red");
        }
        else if($.trim(email)  == ""){
            $("#email").css("border", "2px solid red");
        }
        else if($.trim(password)  == ""){
            $("#password").css("border", "2px solid red");
        }
        else if($.trim(passwordc)  == ""){
            $("#password-confirm").css("border", "2px solid red");
        }
        else if($.trim(password)  != $.trim(passwordc) ){
            $("#password").css("border", "2px solid red");
        }else{
           
                $("#form1").hide();
                $("#next").hide();
                $("#form2").show();
                $("#submit").show();    

        }
       

    });
 
});

</script>
@endsection

@section('footer_scripts')
@if(config('settings.reCaptchStatus'))
<script src='https://www.google.com/recaptcha/api.js'></script>
@endif
@endsection
