<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <div class="navbar-brand">
        <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>
        <a href="{{ route('home') }}"><img src="{{ asset('assets/images/logopdf2.png') }}" width="25"
                alt="Aero"><span class="m-l-10">FARMDAR</span></a>
    </div>
    <div class="menu">
        <ul class="list">
            <li>
                <div class="user-info">
                    <div class=""></div>
                    <div class="detail">

                        @php
                            $user = Auth::user()->name;
                        @endphp
                        <h4>{{ $user }}</h4>

                    </div>
                </div>
            </li>
            <?php if(Auth::user()->role_id == 1){ ?>
            <li class="{{ Request::segment(1) === 'my-profile' ? 'active open' : null }} cup">
                <a href="{{ route('user.profile') }}"><i class="zmdi zmdi-account"></i><span>My Profile</span></a>
            </li>
            <li class="{{ Request::segment(1) === 'my-profile' ? 'active open' : null }} cup">
                <a href="{{ route('user.accounts') }}"><i class="zmdi zmdi-account"></i><span>Accounts</span></a>
            </li>
            <li class="{{ Request::segment(1) === 'app' ? 'active open' : null }} cup">
                <a href="{{ route('user.fields') }}"><i class="zmdi zmdi-map"></i> <span>Fields</span></a>
            </li>
            <li class="{{ Request::segment(1) === 'app' ? 'active open' : null }} cup">
                <a href="{{ route('user.reports') }}"><i class="zmdi zmdi-chart"></i> <span>Reports</span></a>
            </li>
            <?php }else{ ?>
            <li class="{{ Request::segment(1) === 'my-profile' ? 'active open' : null }} cup">
                <a href="{{ route('user.profile') }}"><i class="zmdi zmdi-account"></i><span>My Profile</span></a>
            </li>
            <li class="{{ Request::segment(1) === 'app' ? 'active open' : null }} cup">
                <a href="{{ route('user.map') }}"><i class="zmdi zmdi-pin"></i> <span>Create Field</span></a>
            </li>
            <li class="{{ Request::segment(1) === 'app' ? 'active open' : null }} cup">
                <a href="{{ route('user.fields') }}"><i class="zmdi zmdi-map"></i> <span>Field List</span></a>
            </li>
            <!--<li class="{{ Request::segment(1) === 'app' ? 'active open' : null }} cup">
                <a href="{{ route('user.report_dashboard') }}"><i class="zmdi zmdi-chart"></i> <span>Reports</span></a>
            </li>-->
            <li class="{{ Request::segment(1) === 'app' ? 'active open' : null }} cup">
                <a href="{{ route('user.observer_fields') }}"><i class="zmdi zmdi-format-list-bulleted"></i> <span>Observed List</span></a>
            </li>
            <?php } ?>

            <li class="{{ Request::segment(1) === 'icons' ? 'active open' : null }} cup">

                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="zmdi zmdi-power"></i> <span>Logout</span>
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>

        </ul>
    </div>
</aside>
