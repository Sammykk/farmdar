@extends('user_layout.master')
@section('title', 'Welcome')
@section('parentPageTitle', 'Welcome')
@section('page-style')
    <link rel="stylesheet" href="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css') }}" />
@stop
@section('content')



    <div class="row clearfix">
        <div class="col-md-12 col-lg-12">
            <div class="card" style="background-color: black; opacity: 0.3; z-index: 10; padding:20px">
                @php
                    $user = Auth::user()->name;
                @endphp
                <h5 style="color: white">Welcome to the Farmdar Webapp {{$user}}</h5>
                <h5 style="color: white">In 3 easy steps, you can get a customised quote for your fields.</h5>
                <h5 style="color: rgb(238, 234, 234);"><b>STEP 1</b></h5>
                <h5 style="color: white">Using the field creator, draw an outline around your field and click CONFIRM
                </h5>
                <h5 style="color: rgb(238, 234, 234);"><b>STEP 2</b></h5>
                <h5 style="color: white">Select the reports you would like us to generate
                </h5>
                <h5 style="color: rgb(238, 234, 234);"><b>STEP 3</b></h5>
                <h5 style="color: white">Click GET QUOTE
                </h5>
                <h5 style="color: rgb(3, 3, 3)"><a href="{{route('user.map')}}" >Click Here to start!</a>
                </h5>
            </div>
        </div>

    </div>
@stop

@section('page-script')
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0"></script>
    <script src="{{ asset('assets/bundles/jvectormap.bundle.js') }}"></script>
    <script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-in-mill.js') }}"></script>
    <script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-us-aea-en.js') }}"></script>
    <script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-uk-mill-en.js') }}"></script>
    <script src="{{ asset('assets/plugins/jvectormap/jquery-jvectormap-au-mill.js') }}"></script>
    <script src="{{ asset('assets/js/pages/maps/jvectormap.js') }}"></script>




@stop
